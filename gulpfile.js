/**
 * Gulpfile for Undergraduate Studies Boilerplate
 *
 * @author Josh Hughes (hughesjd@missouri.edu), Undergraduate Studies, University of Missouri
 * @copyright 2016 Curators of the University of Missouri
 */
'use strict'

// Set dist path
var strDistPath                     = 'dist/';

// Include gulp
var objGulp                         = require('gulp');

// Include gulp plugins
var objPlugins                      = {};
objPlugins.autoprefixer             = require('gulp-autoprefixer');
objPlugins.sass                     = require('gulp-ruby-sass');
objPlugins.svgsprite                = require('gulp-svg-sprite');


// Text-only CSS file
objGulp.task('text-only.css', function() 
{    
    // Configuration
    var objConfiguration = {
        autoprefixer: {
            browsers: [
                'last 2 versions',
                'safari 5',
                'ie 8',
                'ie 9',
                'opera 12.1',
                'ios 6',
                'android 4'
            ]
        },
        sass: {
            emitCompileError: true,
            noCache: true,
            style: 'compressed'
        }
    };
    
    // Execute task
    return objPlugins.sass('src/scss/text-only.scss', objConfiguration.sass).on('error', objPlugins.sass.logError)
        .pipe(objPlugins.autoprefixer(objConfiguration.autoprefixer))
        .pipe(objGulp.dest(strDistPath + 'css/'));
});

// SVG Sprite
objGulp.task('svg-sprite', function() 
{    
    // Configuration
    var objConfig = {
        mode: {
            symbol: {
                dest: 'icons/',
                example: true,
                sprite: 'miz-sprite.svg'
            }
        },
        shape: {
            dimension: {
                maxWidth: 60,
                maxHeight: 60
            }
        },
        svg: {
            xmlDeclaration: false,
            doctypeDeclaration: false,
            namespaceIDs: false,
            namespaceClassnames: false,
            dimensionAttributes: false
        }
    };

    objGulp.src([
        strDistPath + 'shared/images/*.svg',
        strDistPath + 'shared/images/**/*.svg',
        ])
    .pipe(objPlugins.svgsprite(objConfig))
    .pipe(objGulp.dest(strDistPath + 'shared/images/'));
});

// Default tasks
objGulp.task('default', ['text-only.css']);