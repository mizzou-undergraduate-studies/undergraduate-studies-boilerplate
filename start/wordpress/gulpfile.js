/**
 * Gulpfile for PROJECT
 *
 * @author Josh Hughes (hughesjd@missouri.edu), Travis Cook (cooktw@missouri.edu), Undergraduate Studies, University of Missouri
 * @copyright 2018 Curators of the University of Missouri
 */
  
(function()
{
    'use strict';
    
    // Set dist path
    var strDistPath                     = 'dist/wp-content/themes/domain.missouri.edu/';

    // Set dev url
    var strDevUrl                       = 'https://domain.lndo.site'; 
    
    // Include gulp
    var objGulp                         = require('gulp');
    
    // Include gulp plugins
    var objPlugins                      = {};
    objPlugins.autoprefixer             = require('gulp-autoprefixer');
    objPlugins.browserSync              = require('browser-sync').create();
    objPlugins.concat                   = require('gulp-concat');
    objPlugins.jshint                   = require('gulp-jshint');
    objPlugins.notify                   = require('gulp-notify');
    objPlugins.sass                     = require('gulp-ruby-sass');
    objPlugins.svgsprite                = require('gulp-svg-sprite');
    objPlugins.uglify                   = require('gulp-uglify');
    
    // Files to run JSHint on
    var aryJSHintFiles                  = ['gulpfile.js'];
    
    // Default tasks
    var aryDefaultTasks                 = [];

    // All CSS files
    objGulp.task('all-css', function() 
    {    
        // Configuration
        var objConfiguration = {
            autoprefixer: {
                browsers: [
                    'last 2 versions',
                    'safari 5',
                    'ie 8',
                    'ie 9',
                    'opera 12.1',
                    'ios 6',
                    'android 4'
                ]
            },
            notify: {
                title: 'Task Successful',
                message: 'All CSS files have been compiled.',
                onLast: true
            },
            sass: {
                emitCompileError: true,
                noCache: true,
                style: 'compressed'
            }
        };
        
        // Execute task
        return objPlugins.sass('src/scss/*.scss', objConfiguration.sass).on('error', objPlugins.sass.logError)
            .pipe(objPlugins.autoprefixer(objConfiguration.autoprefixer))
            .pipe(objGulp.dest(strDistPath + 'css/'))
            .pipe(objPlugins.notify(objConfiguration.notify));
    });
    aryDefaultTasks.push('all-css');
    
    
    // Watch (All CSS Files)
    objGulp.task('watch', ['all-css'], function(done)
    {   
        // Execute task
        objGulp.watch('src/scss/*.scss', ['all-css']);

        objPlugins.browserSync.reload();
        done();

        objPlugins.browserSync.init({
            files: strDistPath + "/css/*.css",
            proxy: strDevUrl
        });
    });

    // SVG Sprite
    objGulp.task('svg-sprite', function() 
    {    
        // Configuration
        var objConfig = {
            mode: {
                symbol: {
                    dest: 'icons/',
                    example: true,
                    sprite: 'miz-sprite.svg'
                }
            },
            shape: {
                dimension: {
                    maxWidth: 60,
                    maxHeight: 60
                }
            },
            svg: {
                xmlDeclaration: false,
                doctypeDeclaration: false,
                namespaceIDs: false,
                namespaceClassnames: false,
                dimensionAttributes: false
            }
        };

        objGulp.src([
            strDistPath + 'images/*.svg',
            strDistPath + 'images/**/*.svg',
            ])
        .pipe(objPlugins.svgsprite(objConfig))
        .pipe(objGulp.dest(strDistPath + 'images/'));
    });
    aryDefaultTasks.push('svg-sprite');
    
    
    // Boilerplate images
    objGulp.task('boilerplate-images', function() 
    {    
        // Execute task
        objGulp.src([
            'bower_components/undergraduate-studies-boilerplate/dist/shared/images/truman-face-2x.png',
            'bower_components/undergraduate-studies-boilerplate/dist/shared/images/truman-face.png',
            'bower_components/undergraduate-studies-boilerplate/dist/shared/images/truman-paws-2x.png',
            'bower_components/undergraduate-studies-boilerplate/dist/shared/images/truman-paws.png',
            'bower_components/undergraduate-studies-boilerplate/dist/shared/images/favicon.ico'
            ])
            .pipe(objGulp.dest(strDistPath + 'images/'));

         // Execute task
         objGulp.src([
            'bower_components/undergraduate-studies-boilerplate/dist/shared/images/campus-hero/campus-01.jpg',
            'bower_components/undergraduate-studies-boilerplate/dist/shared/images/campus-hero/campus-02.jpg',
            'bower_components/undergraduate-studies-boilerplate/dist/shared/images/campus-hero/campus-03.jpg'
            ])
            .pipe(objGulp.dest(strDistPath + 'images/campus-hero/'));
        
        // Execute task
        objGulp.src([
            'bower_components/undergraduate-studies-boilerplate/dist/shared/images/icons/favicon.ico',
            ])
            .pipe(objGulp.dest(strDistPath + 'images/icons/'));

        // Execute task
        objGulp.src([
            'bower_components/undergraduate-studies-boilerplate/dist/shared/images/icons/action/check-circle.svg',
            'bower_components/undergraduate-studies-boilerplate/dist/shared/images/icons/action/check-circle_1x.png',
            'bower_components/undergraduate-studies-boilerplate/dist/shared/images/icons/action/check-circle_2x.png',
            'bower_components/undergraduate-studies-boilerplate/dist/shared/images/icons/action/lock.svg',
            'bower_components/undergraduate-studies-boilerplate/dist/shared/images/icons/action/lock_1x.png',
            'bower_components/undergraduate-studies-boilerplate/dist/shared/images/icons/action/lock_2x.png',
            'bower_components/undergraduate-studies-boilerplate/dist/shared/images/icons/action/new-window.svg',
            'bower_components/undergraduate-studies-boilerplate/dist/shared/images/icons/action/search.svg',
            'bower_components/undergraduate-studies-boilerplate/dist/shared/images/icons/action/search_1x.png',
            'bower_components/undergraduate-studies-boilerplate/dist/shared/images/icons/action/search_2x.png',
            ])
            .pipe(objGulp.dest(strDistPath + 'images/icons/action/'));
        
        // Execute task
        objGulp.src([
            'bower_components/undergraduate-studies-boilerplate/dist/shared/images/icons/alerts/error.svg',
            'bower_components/undergraduate-studies-boilerplate/dist/shared/images/icons/alerts/info.svg',
            'bower_components/undergraduate-studies-boilerplate/dist/shared/images/icons/alerts/warning.svg',
            ])
            .pipe(objGulp.dest(strDistPath + 'images/icons/alerts/'));

        // Execute task
        objGulp.src([
            'bower_components/undergraduate-studies-boilerplate/dist/shared/images/icons/pinned-site/apple-pinned-site.svg',
            'bower_components/undergraduate-studies-boilerplate/dist/shared/images/icons/pinned-site/microsoft-pinned-site.png',
            'bower_components/undergraduate-studies-boilerplate/dist/shared/images/icons/pinned-site/site-icon-dark-grey-76x76.png',
            'bower_components/undergraduate-studies-boilerplate/dist/shared/images/icons/pinned-site/site-icon-dark-grey-120x120.png',
            'bower_components/undergraduate-studies-boilerplate/dist/shared/images/icons/pinned-site/site-icon-dark-grey-152x152.png',
            'bower_components/undergraduate-studies-boilerplate/dist/shared/images/icons/pinned-site/site-icon-dark-grey-180x180.png',
            'bower_components/undergraduate-studies-boilerplate/dist/shared/images/icons/pinned-site/site-icon-dark-grey-600x600.png',
            'bower_components/undergraduate-studies-boilerplate/dist/shared/images/icons/pinned-site/site-icon-dark-grey-1200x630.png',
            'bower_components/undergraduate-studies-boilerplate/dist/shared/images/icons/pinned-site/site-icon-gold-76x76.png',
            'bower_components/undergraduate-studies-boilerplate/dist/shared/images/icons/pinned-site/site-icon-gold-120x120.png',
            'bower_components/undergraduate-studies-boilerplate/dist/shared/images/icons/pinned-site/site-icon-gold-152x152.png',
            'bower_components/undergraduate-studies-boilerplate/dist/shared/images/icons/pinned-site/site-icon-gold-180x180.png',
            'bower_components/undergraduate-studies-boilerplate/dist/shared/images/icons/pinned-site/site-icon-gold-600x600.png',
            'bower_components/undergraduate-studies-boilerplate/dist/shared/images/icons/pinned-site/site-icon-gold-1200x630.png',
            'bower_components/undergraduate-studies-boilerplate/dist/shared/images/icons/pinned-site/site-icon-light-grey-76x76.png',
            'bower_components/undergraduate-studies-boilerplate/dist/shared/images/icons/pinned-site/site-icon-light-grey-120x120.png',
            'bower_components/undergraduate-studies-boilerplate/dist/shared/images/icons/pinned-site/site-icon-light-grey-152x152.png',
            'bower_components/undergraduate-studies-boilerplate/dist/shared/images/icons/pinned-site/site-icon-light-grey-180x180.png',
            'bower_components/undergraduate-studies-boilerplate/dist/shared/images/icons/pinned-site/site-icon-light-grey-600x600.png',
            'bower_components/undergraduate-studies-boilerplate/dist/shared/images/icons/pinned-site/site-icon-light-grey-1200x630.png',
            'bower_components/undergraduate-studies-boilerplate/dist/shared/images/icons/pinned-site/site-icon-tan-76x76.png',
            'bower_components/undergraduate-studies-boilerplate/dist/shared/images/icons/pinned-site/site-icon-tan-120x120.png',
            'bower_components/undergraduate-studies-boilerplate/dist/shared/images/icons/pinned-site/site-icon-tan-152x152.png',
            'bower_components/undergraduate-studies-boilerplate/dist/shared/images/icons/pinned-site/site-icon-tan-180x180.png',
            'bower_components/undergraduate-studies-boilerplate/dist/shared/images/icons/pinned-site/site-icon-tan-600x600.png',
            'bower_components/undergraduate-studies-boilerplate/dist/shared/images/icons/pinned-site/site-icon-tan-1200x630.png',
            ])
            .pipe(objGulp.dest(strDistPath + 'images/icons/pinned-site/'));

        // Execute task
        objGulp.src([
            'bower_components/undergraduate-studies-boilerplate/dist/shared/images/icons/social/facebook.svg',
            'bower_components/undergraduate-studies-boilerplate/dist/shared/images/icons/social/flickr.svg',
            'bower_components/undergraduate-studies-boilerplate/dist/shared/images/icons/social/google.svg',
            'bower_components/undergraduate-studies-boilerplate/dist/shared/images/icons/social/instagram.svg',
            'bower_components/undergraduate-studies-boilerplate/dist/shared/images/icons/social/linkedin.svg',
            'bower_components/undergraduate-studies-boilerplate/dist/shared/images/icons/social/pinterest.svg',
            'bower_components/undergraduate-studies-boilerplate/dist/shared/images/icons/social/snapchat.svg',
            'bower_components/undergraduate-studies-boilerplate/dist/shared/images/icons/social/twitter.svg',
            'bower_components/undergraduate-studies-boilerplate/dist/shared/images/icons/social/youtube.svg',
            ])
            .pipe(objGulp.dest(strDistPath + 'images/icons/social/'));

            // Execute task
            objGulp.src([
                'bower_components/undergraduate-studies-boilerplate/dist/shared/images/icons/standard/computer.svg',
                'bower_components/undergraduate-studies-boilerplate/dist/shared/images/icons/standard/computer_1x.png',
                'bower_components/undergraduate-studies-boilerplate/dist/shared/images/icons/standard/computer_2x.png',
                'bower_components/undergraduate-studies-boilerplate/dist/shared/images/icons/standard/email.svg',
                'bower_components/undergraduate-studies-boilerplate/dist/shared/images/icons/standard/email_1x.png',
                'bower_components/undergraduate-studies-boilerplate/dist/shared/images/icons/standard/email_2x.png',
                'bower_components/undergraduate-studies-boilerplate/dist/shared/images/icons/standard/school.svg',
                'bower_components/undergraduate-studies-boilerplate/dist/shared/images/icons/standard/school_1x.png',
                'bower_components/undergraduate-studies-boilerplate/dist/shared/images/icons/standard/school_2x.png',
                ])
                .pipe(objGulp.dest(strDistPath + 'images/icons/standard/'));

            // Execute task
            objGulp.src([
                'bower_components/undergraduate-studies-boilerplate/dist/shared/images/signatures/university/mizzou-logo-extra-small.svg',
                'bower_components/undergraduate-studies-boilerplate/dist/shared/images/signatures/university/mizzou-logo-large.svg',
                'bower_components/undergraduate-studies-boilerplate/dist/shared/images/signatures/university/mizzou-logo-medium.svg',
                'bower_components/undergraduate-studies-boilerplate/dist/shared/images/signatures/university/mizzou-logo-small.svg',
                'bower_components/undergraduate-studies-boilerplate/dist/shared/images/signatures/university/mu-logo-24.svg',
                'bower_components/undergraduate-studies-boilerplate/dist/shared/images/signatures/university/mu-logo-24.png',
                'bower_components/undergraduate-studies-boilerplate/dist/shared/images/signatures/university/mu-logo-24-stroked.svg',
                'bower_components/undergraduate-studies-boilerplate/dist/shared/images/signatures/university/mu-logo-24-stroked.png',
                'bower_components/undergraduate-studies-boilerplate/dist/shared/images/signatures/university/mu-logo-32.svg',
                'bower_components/undergraduate-studies-boilerplate/dist/shared/images/signatures/university/mu-logo-32.png',
                'bower_components/undergraduate-studies-boilerplate/dist/shared/images/signatures/university/mu-logo-32-stroked.svg',
                'bower_components/undergraduate-studies-boilerplate/dist/shared/images/signatures/university/mu-logo-32-stroked.png',
                'bower_components/undergraduate-studies-boilerplate/dist/shared/images/signatures/university/mu-logo-48.svg',
                'bower_components/undergraduate-studies-boilerplate/dist/shared/images/signatures/university/mu-logo-48.png',
                'bower_components/undergraduate-studies-boilerplate/dist/shared/images/signatures/university/mu-logo-48-stroked.svg',
                'bower_components/undergraduate-studies-boilerplate/dist/shared/images/signatures/university/mu-logo-48-stroked.png',
                'bower_components/undergraduate-studies-boilerplate/dist/shared/images/signatures/university/mu-wordmark-24.svg',
                'bower_components/undergraduate-studies-boilerplate/dist/shared/images/signatures/university/mu-wordmark-24.png',
                'bower_components/undergraduate-studies-boilerplate/dist/shared/images/signatures/university/mu-wordmark-24-white.svg',
                'bower_components/undergraduate-studies-boilerplate/dist/shared/images/signatures/university/mu-wordmark-24-white.png',
                'bower_components/undergraduate-studies-boilerplate/dist/shared/images/signatures/university/mu-wordmark-32.svg',
                'bower_components/undergraduate-studies-boilerplate/dist/shared/images/signatures/university/mu-wordmark-32.png',
                'bower_components/undergraduate-studies-boilerplate/dist/shared/images/signatures/university/mu-wordmark-32-white.svg',
                'bower_components/undergraduate-studies-boilerplate/dist/shared/images/signatures/university/mu-wordmark-32-white.png',
                'bower_components/undergraduate-studies-boilerplate/dist/shared/images/signatures/university/mu-wordmark-48.svg',
                'bower_components/undergraduate-studies-boilerplate/dist/shared/images/signatures/university/mu-wordmark-48.png',
                'bower_components/undergraduate-studies-boilerplate/dist/shared/images/signatures/university/mu-wordmark-48-white.svg',
                'bower_components/undergraduate-studies-boilerplate/dist/shared/images/signatures/university/mu-wordmark-48-white.png',
                'bower_components/undergraduate-studies-boilerplate/dist/shared/images/signatures/university/unit-wordmark-black-large.svg',
                'bower_components/undergraduate-studies-boilerplate/dist/shared/images/signatures/university/unit-wordmark-black-medium.svg',
                'bower_components/undergraduate-studies-boilerplate/dist/shared/images/signatures/university/unit-wordmark-black-small.svg',
                'bower_components/undergraduate-studies-boilerplate/dist/shared/images/signatures/university/unit-wordmark-white-large.svg',
                'bower_components/undergraduate-studies-boilerplate/dist/shared/images/signatures/university/unit-wordmark-white-medium.svg',
                'bower_components/undergraduate-studies-boilerplate/dist/shared/images/signatures/university/unit-wordmark-white-small.svg',
                'bower_components/undergraduate-studies-boilerplate/dist/shared/images/signatures/university/university-wordmark-black-extra-small.svg',
                'bower_components/undergraduate-studies-boilerplate/dist/shared/images/signatures/university/university-wordmark-black-large.svg',
                'bower_components/undergraduate-studies-boilerplate/dist/shared/images/signatures/university/university-wordmark-black-medium.svg',
                'bower_components/undergraduate-studies-boilerplate/dist/shared/images/signatures/university/university-wordmark-black-small.svg',
                'bower_components/undergraduate-studies-boilerplate/dist/shared/images/signatures/university/university-wordmark-white-extra-small.svg',
                'bower_components/undergraduate-studies-boilerplate/dist/shared/images/signatures/university/university-wordmark-white-large.svg',
                'bower_components/undergraduate-studies-boilerplate/dist/shared/images/signatures/university/university-wordmark-white-medium.svg',
                'bower_components/undergraduate-studies-boilerplate/dist/shared/images/signatures/university/university-wordmark-white-small.svg'
                ])
                .pipe(objGulp.dest(strDistPath + 'images/signatures/university/'));
    });
    aryDefaultTasks.push('boilerplate-images');

    // Boilerplate styles
    objGulp.task('boilerplate-styles', function() 
    {    
        // Execute task
        objGulp.src([
            'bower_components/undergraduate-studies-boilerplate/src/scss/style.scss',
            'bower_components/undergraduate-studies-boilerplate/src/scss/editor-style.scss'
            ])
            .pipe(objGulp.dest('src/scss/'));
    });
    aryDefaultTasks.push('boilerplate-styles');
    
    // Boilerplate scripts
    objGulp.task('boilerplate-scripts', function() 
    {    
        // Execute task
        objGulp.src([
            'bower_components/undergraduate-studies-boilerplate/src/js/script.js',
            ])
            .pipe(objGulp.dest('src/js/'));
    });
    aryDefaultTasks.push('boilerplate-scripts');

    // Boilerplate Fonts
    objGulp.task('boilerplate-fonts', function() 
    {    
        objGulp.src([
                'bower_components/undergraduate-studies-boilerplate/src/fonts/clarendon-urw/367C47_0_0.eot',
                'bower_components/undergraduate-studies-boilerplate/src/fonts/clarendon-urw/367C47_1_0.eot',
                'bower_components/undergraduate-studies-boilerplate/src/fonts/clarendon-urw/367C47_2_0.eot',
                'bower_components/undergraduate-studies-boilerplate/src/fonts/clarendon-urw/367C47_2_0.eot',
                'bower_components/undergraduate-studies-boilerplate/src/fonts/clarendon-urw/367C47_0_0.ttf',
                'bower_components/undergraduate-studies-boilerplate/src/fonts/clarendon-urw/367C47_1_0.ttf',
                'bower_components/undergraduate-studies-boilerplate/src/fonts/clarendon-urw/367C47_2_0.ttf',
                'bower_components/undergraduate-studies-boilerplate/src/fonts/clarendon-urw/367C47_3_0.ttf',
                'bower_components/undergraduate-studies-boilerplate/src/fonts/clarendon-urw/367C47_0_0.woff',
                'bower_components/undergraduate-studies-boilerplate/src/fonts/clarendon-urw/367C47_1_0.woff',
                'bower_components/undergraduate-studies-boilerplate/src/fonts/clarendon-urw/367C47_2_0.woff',
                'bower_components/undergraduate-studies-boilerplate/src/fonts/clarendon-urw/367C47_3_0.woff',
                'bower_components/undergraduate-studies-boilerplate/src/fonts/clarendon-urw/367C47_0_0.woff',
                'bower_components/undergraduate-studies-boilerplate/src/fonts/clarendon-urw/367C47_1_0.woff2',
                'bower_components/undergraduate-studies-boilerplate/src/fonts/clarendon-urw/367C47_2_0.woff2',
                'bower_components/undergraduate-studies-boilerplate/src/fonts/clarendon-urw/367C47_3_0.woff2',
        ])
        .pipe(objGulp.dest(strDistPath + 'css/fonts/clarendon-urw/'));

        objGulp.src([
                'bower_components/undergraduate-studies-boilerplate/src/fonts/graphik-condensed-black/GraphikCondensed-Black-Web.eot',
                'bower_components/undergraduate-studies-boilerplate/src/fonts/graphik-condensed-black/GraphikCondensed-Black-Web.woff',
                'bower_components/undergraduate-studies-boilerplate/src/fonts/graphik-condensed-black/GraphikCondensed-Black-Web.woff2',
        ])
        .pipe(objGulp.dest(strDistPath + 'css/fonts/graphik-condensed-black/'));

        objGulp.src([
            'bower_components/undergraduate-studies-boilerplate/src/fonts/open-sans/open-sans-v15-latin-300.woff',
            'bower_components/undergraduate-studies-boilerplate/src/fonts/open-sans/open-sans-v15-latin-300.woff2',
            'bower_components/undergraduate-studies-boilerplate/src/fonts/open-sans/open-sans-v15-latin-300italic.woff',
            'bower_components/undergraduate-studies-boilerplate/src/fonts/open-sans/open-sans-v15-latin-300italic.woff2',
            'bower_components/undergraduate-studies-boilerplate/src/fonts/open-sans/open-sans-v15-latin-regular.woff',
            'bower_components/undergraduate-studies-boilerplate/src/fonts/open-sans/open-sans-v15-latin-regular.woff2',
            'bower_components/undergraduate-studies-boilerplate/src/fonts/open-sans/open-sans-v15-latin-italic.woff',
            'bower_components/undergraduate-studies-boilerplate/src/fonts/open-sans/open-sans-v15-latin-italic.woff2',
            'bower_components/undergraduate-studies-boilerplate/src/fonts/open-sans/open-sans-v15-latin-600.woff',
            'bower_components/undergraduate-studies-boilerplate/src/fonts/open-sans/open-sans-v15-latin-600.woff2',
            'bower_components/undergraduate-studies-boilerplate/src/fonts/open-sans/open-sans-v15-latin-600italic.woff',
            'bower_components/undergraduate-studies-boilerplate/src/fonts/open-sans/open-sans-v15-latin-600italic.woff2',
            'bower_components/undergraduate-studies-boilerplate/src/fonts/open-sans/open-sans-v15-latin-700.woff',
            'bower_components/undergraduate-studies-boilerplate/src/fonts/open-sans/open-sans-v15-latin-700.woff2',
            'bower_components/undergraduate-studies-boilerplate/src/fonts/open-sans/open-sans-v15-latin-700italic.woff',
            'bower_components/undergraduate-studies-boilerplate/src/fonts/open-sans/open-sans-v15-latin-700italic.woff2',
    ])
    .pipe(objGulp.dest(strDistPath + 'css/fonts/open-sans/'));
    });

    aryDefaultTasks.push('boilerplate-fonts');
    
    // Google Analytics
    objGulp.task('google-analytics.js', function() 
    {    
        // Configuration
        var objConfiguration = {
            uglify: {
                mangle: false
            }
        };
        
        // Execute task
        objGulp.src([
            'src/js/config-google-analytics.js', 
            'bower_components/undergraduate-studies-boilerplate/vendor/google-analytics.js'
            ])
            .pipe(objPlugins.concat('google-analytics.js'))
            .pipe(objPlugins.uglify(objConfiguration.uglify))
            .pipe(objGulp.dest(strDistPath + 'js/'));
    });
    aryDefaultTasks.push('google-analytics.js');    
    
    // JSHint
    objGulp.task('jshint', function() 
    {    
        // Configuration
        var objConfiguration = {
            jshint: {
                bitwise: true,
                browser: true,
                curly: true,
                eqeqeq: true,
                freeze: true,
                latedef: true,
                newcap: true,
                noarg: true,
                notypeof: true,
                regexp: true,
                trailing: true,
                unused: true
            }
        };
        
        // Execute task
        objGulp.src(aryJSHintFiles)
            .pipe(objPlugins.jshint(objConfiguration.jshint))
            .pipe(objPlugins.jshint.reporter('default'));
    });
    aryDefaultTasks.push('jshint');
    
    
    // script.js (Menu button functionality)
    objGulp.task('script.js', function() 
    {    
        // Configuration
        var objConfiguration = {
            uglify: {
                mangle: false
            }
        };
        
        // Execute task
        objGulp.src([
            'bower_components/undergraduate-studies-boilerplate/src/js/class-manipulation.js', 
            'bower_components/undergraduate-studies-boilerplate/src/js/dom-traversal.js',
            'bower_components/undergraduate-studies-boilerplate/src/js/mobile-menu-button.js',
            'src/js/script.js'
            ])
            .pipe(objPlugins.concat('script.js'))
            .pipe(objPlugins.uglify(objConfiguration.uglify))
            .pipe(objGulp.dest(strDistPath + 'js/'));
    });
    aryJSHintFiles.push('src/js/script.js');
    aryDefaultTasks.push('script.js');
    
    
    // Text-only CSS file
    objGulp.task('text-only.css', function() 
    {    
        // Execute task
        objGulp.src('bower_components/undergraduate-studies-boilerplate/dist/shared/css/text-only.css')
            .pipe(objGulp.dest(strDistPath + 'css/'));
    });
    aryDefaultTasks.push('text-only.css');
    
    
    // Twig templates
    objGulp.task('twig-templates', function() 
    {    
        // Components - execute task
        objGulp.src([
                'bower_components/undergraduate-studies-boilerplate/dist/wordpress/wp-content/theme/twig/components/block.twig',
                'bower_components/undergraduate-studies-boilerplate/dist/wordpress/wp-content/theme/twig/components/content.twig',
                'bower_components/undergraduate-studies-boilerplate/dist/wordpress/wp-content/theme/twig/components/event.twig',
                'bower_components/undergraduate-studies-boilerplate/dist/wordpress/wp-content/theme/twig/components/footer.twig',
                'bower_components/undergraduate-studies-boilerplate/dist/wordpress/wp-content/theme/twig/components/form-field.twig',
                'bower_components/undergraduate-studies-boilerplate/dist/wordpress/wp-content/theme/twig/components/google-analytics.twig',
                'bower_components/undergraduate-studies-boilerplate/dist/wordpress/wp-content/theme/twig/components/google-cse.twig',
                'bower_components/undergraduate-studies-boilerplate/dist/wordpress/wp-content/theme/twig/components/head-css.twig',
                'bower_components/undergraduate-studies-boilerplate/dist/wordpress/wp-content/theme/twig/components/head-icons.twig',
                'bower_components/undergraduate-studies-boilerplate/dist/wordpress/wp-content/theme/twig/components/head-js.twig',
                'bower_components/undergraduate-studies-boilerplate/dist/wordpress/wp-content/theme/twig/components/head-metadata.twig',
                'bower_components/undergraduate-studies-boilerplate/dist/wordpress/wp-content/theme/twig/components/header-with-university-signature.twig',
                'bower_components/undergraduate-studies-boilerplate/dist/wordpress/wp-content/theme/twig/components/header.twig',
                'bower_components/undergraduate-studies-boilerplate/dist/wordpress/wp-content/theme/twig/components/header-masthead.twig',
                'bower_components/undergraduate-studies-boilerplate/dist/wordpress/wp-content/theme/twig/components/header-ribbon.twig',
                'bower_components/undergraduate-studies-boilerplate/dist/wordpress/wp-content/theme/twig/components/hero.twig',
                'bower_components/undergraduate-studies-boilerplate/dist/wordpress/wp-content/theme/twig/components/hero-video.twig',
                'bower_components/undergraduate-studies-boilerplate/dist/wordpress/wp-content/theme/twig/components/legal-text.twig',
                'bower_components/undergraduate-studies-boilerplate/dist/wordpress/wp-content/theme/twig/components/navigation-pagination.twig',
                'bower_components/undergraduate-studies-boilerplate/dist/wordpress/wp-content/theme/twig/components/navigation.twig',
                'bower_components/undergraduate-studies-boilerplate/dist/wordpress/wp-content/theme/twig/components/outdated-browser.twig',
                'bower_components/undergraduate-studies-boilerplate/dist/wordpress/wp-content/theme/twig/components/person-profile.twig',
                'bower_components/undergraduate-studies-boilerplate/dist/wordpress/wp-content/theme/twig/components/search-form.twig',
                'bower_components/undergraduate-studies-boilerplate/dist/wordpress/wp-content/theme/twig/components/search-results.twig',
                'bower_components/undergraduate-studies-boilerplate/dist/wordpress/wp-content/theme/twig/components/skip-to-content.twig',
                'bower_components/undergraduate-studies-boilerplate/dist/wordpress/wp-content/theme/twig/components/social-media.twig',
                'bower_components/undergraduate-studies-boilerplate/dist/wordpress/wp-content/theme/twig/components/story.twig',
                'bower_components/undergraduate-studies-boilerplate/dist/wordpress/wp-content/theme/twig/components/typekit.twig',
                'bower_components/undergraduate-studies-boilerplate/dist/wordpress/wp-content/theme/twig/components/unit-signature.twig',
                'bower_components/undergraduate-studies-boilerplate/dist/wordpress/wp-content/theme/twig/components/university-signature.twig'
            ])
            .pipe(objGulp.dest(strDistPath + 'inc/templates/components/'));
            
        // Macros - execute task
        objGulp.src([
                'bower_components/undergraduate-studies-boilerplate/dist/wordpress/wp-content/theme/twig/macros/navigation.twig',
                'bower_components/undergraduate-studies-boilerplate/dist/wordpress/wp-content/theme/twig/macros/navigation-dropdowns.twig'
            ])
            .pipe(objGulp.dest(strDistPath + 'inc/templates/macros/'));
            
        // Templates - execute task
        objGulp.src([
                'bower_components/undergraduate-studies-boilerplate/dist/wordpress/wp-content/theme/twig/404.twig',
                'bower_components/undergraduate-studies-boilerplate/dist/wordpress/wp-content/theme/twig/index.twig',
                'bower_components/undergraduate-studies-boilerplate/dist/wordpress/wp-content/theme/twig/main-template.twig',
                'bower_components/undergraduate-studies-boilerplate/dist/wordpress/wp-content/theme/twig/site-template.twig',
                'bower_components/undergraduate-studies-boilerplate/dist/wordpress/wp-content/theme/twig/page.twig',
                'bower_components/undergraduate-studies-boilerplate/dist/wordpress/wp-content/theme/twig/post.twig',
                'bower_components/undergraduate-studies-boilerplate/dist/wordpress/wp-content/theme/twig/search.twig',
                'bower_components/undergraduate-studies-boilerplate/dist/wordpress/wp-content/theme/twig/single-event.twig',
                'bower_components/undergraduate-studies-boilerplate/dist/wordpress/wp-content/theme/twig/single-staff.twig',
                'bower_components/undergraduate-studies-boilerplate/dist/wordpress/wp-content/theme/twig/template-applications.twig',
                'bower_components/undergraduate-studies-boilerplate/dist/wordpress/wp-content/theme/twig/template-archive.twig',
                'bower_components/undergraduate-studies-boilerplate/dist/wordpress/wp-content/theme/twig/page-archive.twig',
            ])
            .pipe(objGulp.dest(strDistPath + 'inc/templates/'));
    });
    aryDefaultTasks.push('twig-templates');
    
    
    // WordPress theme files
    objGulp.task('wordpress-files', function() 
    {    
        objGulp.src([
                'bower_components/undergraduate-studies-boilerplate/dist/wordpress/wp-content/theme/functions-defaults.php',
                'bower_components/undergraduate-studies-boilerplate/dist/wordpress/wp-content/theme/functions.php',
                'bower_components/undergraduate-studies-boilerplate/dist/wordpress/wp-content/theme/options.php',
                'bower_components/undergraduate-studies-boilerplate/dist/wordpress/wp-content/theme/404.php',
                'bower_components/undergraduate-studies-boilerplate/dist/wordpress/wp-content/theme/index.php',
                'bower_components/undergraduate-studies-boilerplate/dist/wordpress/wp-content/theme/page-staff.php',
                'bower_components/undergraduate-studies-boilerplate/dist/wordpress/wp-content/theme/search.php',
                'bower_components/undergraduate-studies-boilerplate/dist/wordpress/wp-content/theme/singular.php',
                'bower_components/undergraduate-studies-boilerplate/dist/wordpress/wp-content/theme/style.css'
            ])
            .pipe(objGulp.dest(strDistPath));
    });
    aryDefaultTasks.push('wordpress-files');
    
    
    // Register default tasks
    objGulp.task('default', aryDefaultTasks);
}());