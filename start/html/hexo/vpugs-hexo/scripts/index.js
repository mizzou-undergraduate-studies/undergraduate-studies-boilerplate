/* global hexo */

"use strict";

const renderer = require("./lib/renderer.js");

hexo.extend.renderer.register("twig", "html", renderer, true);
