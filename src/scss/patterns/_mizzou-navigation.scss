/**
 * Sass CSS Framework: Navigation
 *
 * Requires: mizzou-variables.scss
 *
 * @author Josh Hughes (hughesjd@missouri.edu), Travis Cook (cooktw@missouri.edu), Undergraduate Studies, University of Missouri
 * @copyright 2018 Curators of the University of Missouri
 */

/**
 * Sets up a vertical navigation with custom attributes.
 *
 * @param color $color-border Border color.
 * @param color $color-link Link color.
 * @param color $color-link-hover Link color on hover and focus.
 * @param color $color-link-background Link background color.
 * @param color $color-link-background-hover Link background color on hover and focus.
 * @param color $color-border-hover (Optional) Border color on hover and selected page. Defaults to $color-border.
 * @param color $color-current-page (Optional) Current page color. Defaults to $color-link-hover.
 * @param color $color-current-page-background (Optional) Current page background color. Defaults to $color-link-background-hover.
 * @param color $color-link-background-highlight (Optional) Background color highlight (primarily used for mobile menu). Defaults to $color-link-background.
 * @param color $color-link-background-highlight-hover (Optional) Background color highlight on hover (primarily used for mobile menu). Defaults to $color-link-background-hover.
 * @param color $color-link-parent-hover (Optional) Link color for parent links. Defaults to $color-link.
 * @param color $color-sub-menu-border (Optional) Sub-menu border color. Defaults to $color-border.
 * @param color $color-sub-menu-border-hover (Optional) Sub-menu border color on hover and selected page. Defaults to $color-sub-menu-border.
 * @param color $color-sub-menu-link (Optional) Sub-menu link color. Defaults to $color-link.
 * @param color $color-sub-menu-link-hover (Optional) Sub-menu link color on hover and focus. Defaults to $color-link-hover.
 * @param color $color-sub-menu-link-background (Optional) Sub-menu link background color. Defaults to $color-link-background.
 * @param color $color-sub-menu-link-background-hover (Optional) Sub-menu link background color on hover and focus. Defaults to $color-link-background-hover.
 * @param color $color-sub-menu-link-parent-hover (Optional) Sub-menu link color for parent links. Defaults to $color-sub-menu-link.
 * @param color $color-sub-menu-current-page (Optional) Sub-menu current page color. Defaults to $color-sub-menu-link-hover.
 * @param color $color-sub-menu-current-page-background (Optional) Sub-menu current page background color. Defaults to $color-sub-menu-link-background-hover.
 * @param boolean $include-mobile-menu (Optional) Whether to include mobile menu styles. Defaults to false.
 * @param media query $query-disable-mobile-menu (Optional) Media query at which to disable the mobile menu. Defaults to false.
 */
 @mixin custom-vertical-navigation(
    $color-border,
    $color-link,
    $color-link-hover,
    $color-link-background,
    $color-link-background-hover,
    $color-border-hover:                            $color-border,
    $color-current-page:                            $color-link-hover,
    $color-current-page-background:                 $color-link-background-hover,
    $color-link-background-highlight:               $color-link-background,
    $color-link-background-highlight-hover:         $color-link-background-hover,
    $color-link-parent-hover:                       $color-link,
    $color-sub-menu-border:                         $color-border,
    $color-sub-menu-border-hover:                   $color-sub-menu-border,
    $color-sub-menu-link:                           $color-link,
    $color-sub-menu-link-hover:                     $color-link-hover,
    $color-sub-menu-link-background:                $color-link-background,
    $color-sub-menu-link-background-hover:          $color-link-background-hover,
    $color-sub-menu-link-parent-hover:              $color-sub-menu-link,
    $color-sub-menu-current-page:                   $color-sub-menu-link-hover,
    $color-sub-menu-current-page-background:        $color-sub-menu-link-background,
    $include-mobile-menu:                           false,
    $query-disable-mobile-menu:                     false
)
{
    background: $color-link-background;
    color: $color-link;

    .nav__list
    {
        padding: 0;
        list-style: none;
        
        // Disable the border for the first two levels
        > .nav__item:first-child
        {
            > .nav__current-page,
            > .nav__link
            {
                border-top-width: 0;
                margin-top: 0;
            }
        }
        
        // Re-enable the border for the third level
        .nav__list .nav__list > .nav__item:first-child
        {
            > .nav__current-page,
            > .nav__link
            {
                border-top-width: 1px;
                margin-top: -1px;
            }
        }
    }
    
    // Selectively remove the bottom border
    .nav__container > .nav__list > .nav__item:last-child,
    .nav__container > .nav__list > .nav__item > .nav__list > .nav__item:last-child,
    .nav__container > .nav__list > .nav__item > .nav__list > .nav__item:last-child .nav__item:last-child
    {
        // Take away the bottom border on last links, and current page items
        > .nav__current-page,
        > .nav__link
        {
            border-bottom-width: 0;
        }
    
        // Reinstate it for parents, however
        > .nav__current-page.nav__current-page--parent,
        > .nav__link.nav__link--parent
        {
            border-bottom-width: 1px;
        }
    }
    
    .nav__current-page,
    .nav__link
    {
        display: block;
        margin: -1px 16px 0 16px;
        padding: 8px 0;
        text-decoration: none;
        font-size: 18px;
        font-weight: $font-weight-bold;
        color: $color-link;
        border-top: 1px solid $color-border;
        border-bottom: 1px solid $color-border;
    }
    
    .nav__link
    {
        &:hover,
        &:focus,
        &:active
        {
            background: $color-link-background-hover;
            color: $color-link-hover;
            border-color: $color-border-hover;
            position: relative;
        
            @if ($color-link-background-hover != $color-link-background) {
                margin-left: 0;
                margin-right: 0;
                padding-left: 16px;
                padding-right: 16px;
            }
        }
        
        &.nav__link--parent
        {
            &:hover,
            &:focus,
            &:active
            {
                color: $color-link-parent-hover;
            }
        }
    }
    
    .nav__current-page,
    .nav__link.nav__link--parent
    {
        background: $color-current-page-background;
        color: $color-current-page;
        border-color: $color-border-hover;
        position: relative;
        
        @if ($color-current-page-background != $color-link-background) {
            margin-left: 0;
            margin-right: 0;
            padding-left: 16px;
            padding-right: 16px;
        }
    }
    
    .nav__item
    {
        // Sub menus
        .nav__list
        {
            background: $color-sub-menu-link-background;
            border-bottom: 1px solid $color-border-hover;
            position: relative;
            list-style: none;

            .nav__link
            {
                color: $color-sub-menu-link;
                border-color: $color-sub-menu-border;

                &:hover,
                &:focus,
                &:active
                {
                    background: $color-sub-menu-link-background-hover;
                    color: $color-sub-menu-link-hover;
                    border-color: $color-sub-menu-border-hover;
                
                    @if ($color-sub-menu-link-background-hover != $color-sub-menu-link-background) {
                        margin-left: 0;
                        margin-right: 0;
                        padding-left: 16px;
                        padding-right: 16px;
                    } @else {
                        margin-left: 16px;
                        margin-right: 16px;
                        padding-left: 0;
                        padding-right: 0;
                    }
                }
                
                &.nav__link--parent
                {
                    &:hover,
                    &:focus,
                    &:active
                    {
                        color: $color-sub-menu-link-parent-hover;
                    }
                }
            }
            
            .nav__current-page,
            .nav__link.nav__link--parent
            {
                background: $color-sub-menu-current-page-background;
                color: $color-sub-menu-current-page;
                border-color: $color-sub-menu-border-hover;
                
                @if ($color-sub-menu-current-page-background != $color-sub-menu-link-background) {
                    margin-left: 0;
                    margin-right: 0;
                    padding-left: 16px;
                    padding-right: 16px;
                } @else {
                    margin-left: 16px;
                    margin-right: 16px;
                    padding-left: 0;
                    padding-right: 0;
                }
            }
            
            .nav__list
            {
                border-bottom: 0;
                background: none;
                padding-left: 16px;
            }
        }
    }
    
    // Mobile menu
    @if ($include-mobile-menu) {
        
        &.nav--hide 
        {
            .nav__mobile-menu-button-header:before,
            .nav__mobile-menu-button-header:after,
            .nav__mobile-menu-button-link:after
            {
                transform: none;
            }
            
            .nav__list
            {
                display: none;
                
                @if ($query-disable-mobile-menu != false) {
                    @media #{$query-disable-mobile-menu}
                    {
                        display: block;
                    }
                }
            }
        }
        
        .nav__list
        {
            height: auto;
        }
        
        .nav__mobile-menu-button
        {
            border-bottom: 1px solid $color-border;
            
            @if (lightness($color-border) <= lightness($color-link-background-highlight)) {
                border-top: 1px solid $color-border;
            }
            
            @if ($query-disable-mobile-menu != false) {
                @media #{$query-disable-mobile-menu}
                {
                    display: none;
                }
            }
        }
        
        .nav__mobile-menu-button-link
        {
            position: relative;
            cursor: pointer;
            min-width: 56px;
            text-transform: uppercase;
            padding: 16px 16px 16px 56px;
            min-height: 54px;
            text-decoration: none;
            display: block;
            font-weight: $font-weight-bold;
            
            &:hover,
            &:focus
            {
                background-color: $color-link-background-hover;
                            
                .nav__mobile-menu-button-header
                {
                    color: $color-link-hover;
                }
                
                .nav__mobile-menu-button-header:before,
                .nav__mobile-menu-button-header:after,
                &:after
                {
                    background: $color-link-hover;
                }
            }
        }
        
        .nav__mobile-menu-button-header
        {
            font-size: 18px;
            line-height: 1.2;
            color: $color-link;
            margin: 0;
            display: block;
        }
        
        // Hamburger button bars
        .nav__mobile-menu-button-header:before,
        .nav__mobile-menu-button-header:after,
        .nav__mobile-menu-button-link:after
        {
            content: ' ';
            display: block;
            width: 24px;
            height: 4px;
            border-radius: 2px;
            background: $color-link;
            position: absolute;
            left: 16px;
        }
        
        // Top and bottom bar
        .nav__mobile-menu-button-header:before,
        .nav__mobile-menu-button-link:after
        {
            transition: 0.1s transform linear;
        }
        
        // Top bar
        .nav__mobile-menu-button-header:before
        {
            top: 16px;
            transform: translateY(8px) rotate(45deg);
        }
        
        // Middle bar
        .nav__mobile-menu-button-header:after
        {
            top: 24px;
            transform: scale(0);
        }
        
        // Bottom bar
        .nav__mobile-menu-button-link:after
        {
            top: 32px;      
            transform: translateY(-8px) rotate(-45deg);
        }
    }
}

/**
 * Sets up vertical navigation.
 *
 * @param string $color (Optional) Navigation color. Can be 'gold', 'tan', or 'grey'. Defaults to 'gold'.
 * @param boolean $include-mobile-menu (Optional) Whether to include mobile menu styles. Defaults to false.
 * @param media query $query-disable-mobile-menu (Optional) Media query at which to disable the mobile menu. Defaults to false.
 */
@mixin vertical-navigation(
    $color:                         'gold',
    $include-mobile-menu:           false,
    $query-disable-mobile-menu:     false
)
{
    @if ($color == 'tan') {
        @include custom-vertical-navigation(
            $color-border: $color-gold-500,
            $color-link: $color-grey-600,
            $color-link-background-highlight: $color-gold-100,
            $color-link-background: $color-gold-200,
            $color-link-hover: #000,
            $color-link-background-highlight-hover: #fff,
            $color-link-background-hover: $color-gold-100,
            $color-sub-menu-border: $color-gold-200,
            $color-sub-menu-link: $color-grey-600,
            $color-sub-menu-link-background: #fff,
            $color-sub-menu-link-hover: #000,
            $color-sub-menu-link-background-hover: $color-gold-100,
            $include-mobile-menu: $include-mobile-menu,
            $query-disable-mobile-menu: $query-disable-mobile-menu
        );
    } @else if ($color == 'grey') {
        @include custom-vertical-navigation(
            $color-border: $color-grey-600,
            $color-border-hover: #000,
            $color-link: #fff,
            $color-link-background-highlight: $color-grey-400,
            $color-link-background: $color-grey-500,
            $color-link-hover: $color-gold-400,
            $color-link-background-highlight-hover: $color-grey-600,
            $color-link-background-hover: #000,
            $color-sub-menu-border: #000,
            $color-sub-menu-link: #fff,
            $color-sub-menu-link-background: $color-grey-600,
            $color-sub-menu-link-hover: $color-gold-300,
            $color-sub-menu-link-background-hover: #000,
            $include-mobile-menu: $include-mobile-menu,
            $query-disable-mobile-menu: $query-disable-mobile-menu
        );
    } @else if ($color == 'black') {
        @include custom-vertical-navigation(
            $color-border: $color-grey-600,
            $color-border-hover: #000,
            $color-link: #fff,
            $color-link-background-highlight: $color-grey-600,
            $color-link-background: #000,
            $color-link-hover: $color-gold-400,
            $color-link-background-highlight-hover: $color-grey-500,
            $color-link-background-hover: $color-grey-600,
            $color-sub-menu-border: #000,
            $color-sub-menu-link: #fff,
            $color-sub-menu-link-background: $color-grey-500,
            $color-sub-menu-link-hover: $color-gold-300,
            $color-sub-menu-link-background-hover: $color-grey-600,
            $include-mobile-menu: $include-mobile-menu,
            $query-disable-mobile-menu: $query-disable-mobile-menu
        );
    } @else { // gold
        @include custom-vertical-navigation(
            $color-border: $color-gold-500,
            $color-link: $color-grey-600,
            $color-link-background-highlight: $color-gold-200,
            $color-link-background: $color-mu-gold,
            $color-link-hover: #000,
            $color-link-background-highlight-hover: #fff,
            $color-link-background-hover: lighten($color-mu-gold, 9%),
            $color-sub-menu-border: $color-gold-400,
            $color-sub-menu-link: $color-grey-600,
            $color-sub-menu-link-background: $color-gold-200,
            $color-sub-menu-link-hover: #000,
            $color-sub-menu-link-background-hover: $color-gold-100,
            $include-mobile-menu: $include-mobile-menu,
            $query-disable-mobile-menu: $query-disable-mobile-menu
        );
    }
}

/**
 * Sets up a horizontal navigation with custom attributes.
 *
 * @param color $color-border Border color.
 * @param color $color-link Link color.
 * @param color $color-link-hover Link color on hover and focus.
 * @param color $color-link-background-highlight Background gradient top color.
 * @param color $color-link-background Background gradient bottom color.
 * @param color $color-link-background-hover-highlight Link background gradient on hover and focus top color.
 * @param color $color-link-background-hover Link background gradient on hover and focus bottom color.
 * @param color $color-current-page (Optional) Current page color. Defaults to $color-link-hover.
 * @param color $color-current-page-background-highlight (Optional) Current page background gradient top color. Defaults to $color-link-background-hover-highlight.
 * @param color $color-current-page-background (Optional) Current page background gradient bottom color. Defaults to $color-link-background-hover.
* @param boolean $include-dropdown (Optional) Whether to have dropdown navigation. Defaults to false.
 */
@mixin custom-horizontal-navigation(
    $color-border,
    $color-link,
    $color-link-hover,
    $color-link-background-highlight,
    $color-link-background,
    $color-link-background-hover-highlight,
    $color-link-background-hover,
    $color-border-hover:                            $color-border,
    $color-current-page:                            $color-link-hover,
    $color-current-page-background-highlight:       $color-link-background-hover-highlight,
    $color-current-page-background:                 $color-link-background-hover,
    $color-link-parent-hover:                       $color-link,
    $color-sub-menu-border:                         $color-border,
    $color-sub-menu-border-hover:                   $color-sub-menu-border,
    $color-sub-menu-link:                           $color-link,
    $color-sub-menu-link-hover:                     $color-link-hover,
    $color-sub-menu-link-background:                $color-link-background,
    $color-sub-menu-link-background-hover:          $color-link-background-hover,
    $color-sub-menu-link-parent-hover:              $color-sub-menu-link,
    $color-sub-menu-current-page:                   $color-sub-menu-link-hover,
    $color-sub-menu-current-page-background:        $color-sub-menu-link-background,
    $include-dropdown:                              false
)
{
    @include clear-fix;
    position: relative;
    background-color: $color-link-background;

    @if ($include-dropdown == true) {
        .nav__list
        {
            list-style: none;
            padding: 0;
        }
        
        .nav__link,
        .nav__current-page
        {
            font-weight: $font-weight-bold;
            text-decoration: none;
            font-size: 16px;
            display: block;
            padding: 10px .5em;
            margin: 0 0 0 -1px;
            border: 0;
            border-left: 1px solid $color-border;
            border-right: 1px solid $color-border;
        }
        
        .nav__link
        {
            color: $color-link;
            border-image: linear-gradient(to bottom, transparentize($color-border, 1), $color-border, transparentize($color-border, 1)) 0 1 repeat;
            
            &:hover,
            &:focus,
            &:active
            {
                color: $color-link-hover;
                border-top: 0;
                border-image: none;
                background-color: $color-link-background-hover;
                margin: 0 0 0 -1px;
                padding-left: .5em;
                padding-right: .5em;
            }
        }
        
        .nav__current-page
        {
            color: $color-current-page;
            background-color: $color-current-page-background;
        }

        .nav__link--parent
        {
            color: $color-current-page;
            background-color: $color-current-page-background;
            padding-left: .5em !important;
            padding-right: .5em !important;
        }
        
        .nav__item
        {
            position: relative;
            display: inline-block;
            padding: 0;
            
            &:first-child
            {
                .nav__link
                {
                    border-left: 1px solid $color-border;

                    &:active,
                    &:focus
                    {
                        border-left-width: 1px;
                    }
                }
                
                .nav__current-page
                {
                    margin-left: 0;
                }
            }
            
            &:last-child
            {
                .nav__link
                {
                    border-right-width: 0;
                    
                    &:hover,
                    &:focus,
                    &:active
                    {
                        border-right-width: 1px;
                    }
                }
            }
            
            &:hover
            {
                .nav__list
                {
                    visibility: visible;
                }
            }
            
            // Drop-down sub-menu
            .nav__list
            {
                visibility: hidden;
                z-index: 100;
                position: absolute;
                width: 200px;
                min-width: 100%;
                transition-property: transform, opacity, visibility, border-color;
                transition-duration: .3s;
                list-style: none;
                padding: 0;
                background: $color-sub-menu-link-background;

                .nav__item
                {
                    display: block;
                    text-align: left;
                    position: static;
                    
                    .nav__link
                    {
                        color: $color-sub-menu-link;
                        border-width: 0;
                        padding: 10px .5em;
                        margin: 0;
                        width: 100%;

                        &:hover,
                        &:focus,
                        &:active
                        {
                            color: $color-link-hover;
                            border-top: 0;
                            border-image: none;
                            background-color: $color-link-background-hover;
                            margin: 0 0 0 -1px;
                        }
                    }

                    .nav__current-page
                    {
                        padding-left: .5em;
                        padding-right: .5em;
                        margin-right: initial;
                    }
                }
            }
        }
    }

    @if ($include-dropdown == false) {
        .nav__list
        {
            list-style: none;
            padding: 0;
        }
        
        .nav__link,
        .nav__current-page
        {
            font-weight: $font-weight-bold;
            text-decoration: none;
            font-size: 16px;
            display: block;
            padding: 10px .5em;
            margin: 0 0 0 -1px;
            float: left;
            border: 0;
            border-left: 1px solid $color-border;
            border-right: 1px solid $color-border;
        }
        
        .nav__link
        {
            color: $color-link;
            border-image: linear-gradient(to bottom, transparentize($color-border, 1), $color-border, transparentize($color-border, 1)) 0 1 repeat;
            
            &:hover,
            &:focus,
            &:active
            {
                color: $color-link-hover;
                border-top: 0;
                border-image: none;
                background-color: $color-link-background-hover;
                margin: 0 0 0 -1px;
                padding-left: .5em;
                padding-right: .5em;
            }
        }
        
        .nav__current-page
        {
            color: $color-current-page;
            background-color: $color-current-page-background;
        }

        .nav__link--parent
        {
            color: $color-current-page;
            background-color: $color-current-page-background;
            padding-left: .5em !important;
            padding-right: .5em !important;
        }
        
        .nav__item
        {
            &:first-child
            {
                .nav__link
                {
                    border-left: 0px solid $color-border;

                    &:active,
                    &:focus
                    {
                        border-left-width: 1px;
                    }
                }
                
                .nav__current-page
                {
                    margin-left: 0;
                }
            }
            
            &:last-child
            {
                .nav__link
                {
                    border-right-width: 0;
                    
                    &:hover,
                    &:focus,
                    &:active
                    {
                        border-right-width: 1px;
                    }
                }
            }
            
            // Disable further levels
            .nav__list
            {
                display: none;
            }
        }
    }
}

/**
 * Sets up horizontal navigation.
 *
 * @param string $color (Optional) Navigation color. Can be 'gold', 'tan', or 'grey'. Defaults to 'gold'.
 * @param boolean $include-mobile-menu (Optional) Whether to include mobile menu styles. Defaults to false.
 * @param media query $query-disable-mobile-menu (Optional) Media query at which to disable the mobile menu. Defaults to false.
 * @param boolean $include-dropdown (Optional) Whether to have dropdown navigation. Defaults to false.
 */
@mixin horizontal-navigation(
    $color:                         'gold',
    $include-dropdown:              false
)
{
    @if ($color == 'tan') {
        @include custom-horizontal-navigation(
            $color-border: $color-gold-500,
            $color-link: $color-brown-500,
            $color-link-hover: $color-brown-600,
            $color-link-background: $color-gold-200,
            $color-link-background-highlight: $color-gold-100,
            $color-link-background-hover: $color-gold-100,
            $color-link-background-hover-highlight: #fff,
            $include-dropdown: $include-dropdown
        );
    } @else if ($color == 'grey') {
        @include custom-horizontal-navigation(
            $color-border: #000,
            $color-link: #fff,
            $color-link-hover: $color-gold-400,
            $color-link-background: $color-grey-500,
            $color-link-background-highlight: $color-grey-400,
            $color-link-background-hover: #000,
            $color-link-background-hover-highlight: $color-grey-600,
            $include-dropdown: $include-dropdown
        );
    } @else if ($color == 'black') {
        @include custom-horizontal-navigation(
            $color-border: $color-grey-500,
            $color-link: #fff,
            $color-link-hover: $color-gold-400,
            $color-link-background: #000,
            $color-link-background-highlight: $color-grey-600,
            $color-link-background-hover: $color-grey-600,
            $color-link-background-hover-highlight: $color-grey-500,
            $include-dropdown: $include-dropdown
        );
    } @else if ($color == 'white') {
        @include custom-horizontal-navigation(
            $color-border: #000,
            $color-link: #000,
            $color-link-hover: $color-brown-500,
            $color-link-background-highlight: #fff,
            $color-link-background: #fff,
            $color-link-background-hover: #fff,
            $color-link-background-hover-highlight: #fff,
            $include-dropdown: $include-dropdown
        );
    } @else { // gold
        @include custom-horizontal-navigation(
            $color-border: $color-gold-500,
            $color-link: $color-grey-600,
            $color-link-hover: #000,
            $color-link-background: $color-mu-gold,
            $color-link-background-highlight: $color-gold-300,
            $color-link-background-hover: lighten($color-mu-gold, 9%),
            $color-link-background-hover-highlight: #fff,
            $include-dropdown: $include-dropdown
        );
    }
}