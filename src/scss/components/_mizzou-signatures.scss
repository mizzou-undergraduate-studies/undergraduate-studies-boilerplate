/**
 * Sass CSS Framework: Signatures
 *
 * Requires: mizzou-variables.scss, mizzou-base.scss
 *
 * @author Josh Hughes (hughesjd@missouri.edu), Travis Cook (cooktw@missouri.edu), Undergraduate Studies, University of Missouri
 * @copyright 2018 Curators of the University of Missouri
 */

/**
 * Sets up a unit signature (should only be fired within a .unit-signature container).
 *
 * @param unit $name-width Width of the unit name image.
 * @param unit $name-height Height of the unit name image. Should be half the actual height, as it's a rollover sprite.
 * @param string $name-filename-prefix (Optional) Prefix for unit name file. Defaults to 'unit-name'.
 * @param string $size (Optional) Signature size. Can be 'small', 'medium', or 'large'. Defaults to 'small'.
 * @param string $color (Optional) Text color. Can be 'black' or 'white'. Defaults to 'black'.
 * @param boolean $include-print-style (Optional) Whether to set the images up for printing. Defaults to false.
 */
 @mixin unit-signature(
    $name-width,
    $name-height,
    $name-filename-prefix:      'unit-name',
    $size:                      'small',
    $color:                     'black',
    $include-print-style:       false
)
{
    // Set names
    $name-filename:             'signatures/unit/#{$name-filename-prefix}-#{$color}-#{$size}.svg';
    $wordmark-filename:         'signatures/university/unit-wordmark-#{$color}-#{$size}.svg';
    $logo-filename:             'signatures/university/mizzou-logo-#{$size}.svg';
    
    // Set dimensions
    
    // $size == 'small'
    $logo-width:                34px;
    $logo-height:               38px;
    $logo-space:                10px;
    $wordmark-width:            150px;
    $wordmark-height:           19px;
    
    @if ($size == 'large') {
        $logo-width:            66px;
        $logo-height:           74px;
        $logo-space:            20px;
        $wordmark-width:        246px;
        $wordmark-height:       29px;
    } @else if ($size == 'medium') {
        $logo-width:            50px;
        $logo-height:           56px;
        $logo-space:            15px;
        $wordmark-width:        185px;
        $wordmark-height:       23px;
    }
    
    // Figure out what the height of the text is with the logo height subtracted
    $text-height-minus-logo-height: $name-height + $wordmark-height - $logo-height;
    
    position: relative;
    min-height: $logo-height;
    
    .unit-signature__name
    {
        position: relative;
        margin: 0 0 0 $logo-width;
        padding-left: $logo-space;
        
        span,
        a
        {
            @if ($include-print-style == false) {
                @include replace-with-image(
                    $filename: $name-filename,
                    $width: $name-width,
                    $height: $name-height
                );
            } @else {
                @include print-image(
                    $filename: $name-filename,
                    $width: $name-width,
                    $height: $name-height
                );
            }
        }
        
        a:hover,
        a:focus,
        a:active
        {
            background-position: 0 (-$name-height);
        }
    }
    
    .unit-signature__wordmark
    {
        margin: 0 0 0 $logo-width;
        padding-left: $logo-space;
        
        span,
        a
        {
            @if ($include-print-style == false) {
                @include replace-with-image(
                    $filename: $wordmark-filename,
                    $width: $wordmark-width,
                    $height: $wordmark-height
                );
            } @else {
                @include print-image(
                    $filename: $wordmark-filename,
                    $width: $wordmark-width,
                    $height: $wordmark-height
                );
            }
        }
        
        a:hover,
        a:focus,
        a:active
        {
            background-position: 0 (-$wordmark-height);
        }
    }
    
    .unit-signature__logo
    {
        position: relative;
        left: -1px;
        margin: 0;
        width: $logo-width;
        height: $logo-height;
        
        @if ($text-height-minus-logo-height > 0) {
            margin-top: -$logo-height;
            top: -$text-height-minus-logo-height;
        } @else {
            margin-top: -($name-height + $wordmark-height);
        }
        
        a
        {
            @if ($include-print-style == false) {
                @include replace-with-image(
                    $filename: $logo-filename,
                    $width: $logo-width,
                    $height: $logo-height
                );
            } @else {
                @include print-image(
                    $filename: $logo-filename,
                    $width: $logo-width,
                    $height: $logo-height
                );
            }
        }
    }
}

/**
 * Sets up a university signature.
 *
 * @param string $size (Optional) Signature size. Can be 'extra-small', 'small', 'medium', or 'large'. Defaults to 'small'.
 * @param string $color (Optional) Text color. Can be black or white. Defaults to black.
 * @param boolean $include-print-style (Optional) Whether to set the images up for printing. Defaults to false.
 */
@mixin university-signature(
    $size:                          'small',
    $color:                         'black',
    $include-print-style:           false
)
{
    // Set names
    $wordmark-filename:             'signatures/university/university-wordmark-#{$color}-#{$size}.svg';
    $logo-filename:                 'signatures/university/mizzou-logo-#{$size}.svg';
    
    // Set dimensions
    
    // $size == 'small'
    $logo-width:                    34px;
    $logo-height:                   38px;
    $logo-space:                    10px;
    $wordmark-width:                229px;
    $wordmark-height:               27px;
    $wordmark-top:                  7px;
    
    @if ($size == 'large') {
        $logo-width:                66px;
        $logo-height:               74px;
        $logo-space:                20px;
        $wordmark-width:            478px;
        $wordmark-height:           56px;
        $wordmark-top:              12px;
    } @else if ($size == 'medium') {
        $logo-width:                50px;
        $logo-height:               56px;
        $logo-space:                15px;
        $wordmark-width:            334px;
        $wordmark-height:           40px;
        $wordmark-top:              9px;
    } @else if ($size == 'extra-small')  {
        $logo-width:                26px;
        $logo-height:               29px;
        $logo-space:                7px;
        $wordmark-width:            177px;
        $wordmark-height:           21px;
        $wordmark-top:              7px;
    }
    
    position: relative;
    min-height: $logo-height;
    
    .university-signature__wordmark
    {
        position: relative;
        margin: 0 0 0 $logo-width;
        padding-left: $logo-space;
        top: $wordmark-top;
        
        span,
        a
        {
            @if ($include-print-style == false) {
                @include replace-with-image(
                    $filename: $wordmark-filename,
                    $width: $wordmark-width,
                    $height: $wordmark-height
                );
            } @else {
                @include print-image(
                    $filename: $wordmark-filename,
                    $width: $wordmark-width,
                    $height: $wordmark-height
                );
            }
        }
        
        a:hover,
        a:focus,
        a:active
        {
            background-position: 0 (-$wordmark-height);
        }
    }
    
    .university-signature__logo
    {
        position: relative;
        left: -1px;
        width: $logo-width;
        height: $logo-height;
        margin: -$wordmark-height 0 0 0;
        
        a
        {
            @if ($include-print-style == false) {
                @include replace-with-image(
                    $filename: $logo-filename,
                    $width: $logo-width,
                    $height: $logo-height
                );
            } @else {
                @include print-image(
                    $filename: $logo-filename,
                    $width: $logo-width,
                    $height: $logo-height
                );
            }
        }
    }
}

.miz-comp-ribbon
{
    flex-direction: column;
    display: flex;
    flex-wrap: nowrap;
    justify-content: flex-start;
    padding: 15px 0;

    @media #{$query-784}
    {
        flex-direction: row;
    }
}

.miz-comp-ribbon__signature
{
    align-self: center;
    margin-bottom: 16px;

    @media #{$query-784}
    {
        flex: 0 0 50%;
        align-self: flex-start;
        margin-bottom: 0px;
    }
    
}

.mu-sig-24
{
    .logo
    {
        a
        {
            width: 26px;
            height: 29px;
            margin-left: -1px;
            float: left;
            margin-bottom: 0;
            background-image: url(../images/signatures/university/mu-logo-24-stroked.svg);
        }
    }

    .wordmark
    {
        margin-left: 32px;
        padding-top: 7px;
        margin-bottom: 0;

        a
        {
            width: 177px;
            height: 21px;
            background-image: url(../images/signatures/university/mu-wordmark-24.svg);

            &:hover, &:active, &:active
            {
                background-position: left -21px;
            }
        }
    }

    .logo, .wordmark
    {
        margin-top: 0;
        margin-bottom: 0;

        a
        {
            display: block;
            text-indent: 120%;
            white-space: nowrap;
            overflow: hidden;
            padding: 0;
            background-position: left 0;
            background-repeat: no-repeat;

            &::after
            {
                text-indent: 0;
                display: block;
                position: absolute;
                top: 0;
                left: 0;
            }
        }
    }
    &.reverse
    {
        .logo
        {
            a
            {
                background-image: url(../images/signatures/university/mu-logo-24-stroked.svg);
            }
        }

        .wordmark
        {
            a
            {
                background-image: url(../images/signatures/university/mu-wordmark-24-white.svg);
            }
        }
    }
}

.mu-sig-32
{
    .logo
    {
        a
        {
            width: 34px;
            height: 38px;
            margin-left: -1px;
            float: left;
            margin-bottom: 9px;
            background-image: url(../images/signatures/university/mu-logo-32-stroked.svg);
        }
    }

    .wordmark
    {
        margin-left: 42px;
        padding-top: 7px;
        margin-bottom: 10px;

        a
        {
            width: 229px;
            height: 27px;
            background-image: url(../images/signatures/university/mu-wordmark-32.svg);

            &:hover, &:active, &:active
            {
                background-position: left -27px;
            }
        }
    }

    .logo, .wordmark
    {
        margin-top: 0;
        margin-bottom: 0;

        a
        {
            display: block;
            text-indent: 120%;
            white-space: nowrap;
            overflow: hidden;
            padding: 0;
            background-position: left 0;
            background-repeat: no-repeat;

            &::after
            {
                text-indent: 0;
                display: block;
                position: absolute;
                top: 0;
                left: 0;
            }
        }
    }

    &.reverse
    {
        .logo
        {
            a
            {
                background-image: url(../images/signatures/university/mu-logo-32-stroked.svg);
            }
        }

        .wordmark
        {
            a
            {
                background-image: url(../images/signatures/university/mu-wordmark-32-white.svg);
            }
        }
    }
}

.mu-sig-48
{
    .logo
    {
        a
        {
            width: 26px;
            height: 29px;
            margin-left: -1px;
            float: left;
            margin-bottom: 0;
            background-image: url(../images/signatures/university/mu-logo-48-stroked.svg);
        }
    }

    .wordmark
    {
        margin-left: 32px;
        padding-top: 7px;
        margin-bottom: 0;

        a
        {
            width: 177px;
            height: 21px;
            background-image: url(../images/signatures/university/mu-wordmark-48.svg);

            &:hover, &:active, &:active
            {
                background-position: left -21px;
            }
        }
    }

    .logo, .wordmark
    {
        margin-top: 0;
        margin-bottom: 0;

        a
        {
            display: block;
            text-indent: 120%;
            white-space: nowrap;
            overflow: hidden;
            padding: 0;
            background-position: left 0;
            background-repeat: no-repeat;

            &::after
            {
                text-indent: 0;
                display: block;
                position: absolute;
                top: 0;
                left: 0;
            }
        }
    }

    &.reverse
    {
        .logo
        {
            a
            {
                background-image: url(../images/signatures/university/mu-logo-48-stroked.svg);
            }
        }

        .wordmark
        {
            a
            {
                background-image: url(../images/signatures/university/mu-wordmark-48-white.svg);
            }
        }
    }
}

.miz-comp-masthead 
{
    padding:.5em 0;

    & h1 
    {
        margin-bottom:0;
        padding:.5em 0
    }
    
    & h1 a 
    {
        color:$color-grey-600;
        text-decoration: none;
        font-size: 1.8em;
        font-weight:400;
        line-height:1.2em;
        letter-spacing: -0.0125em;    
    }

    & h1 a:hover 
    {
        color: $color-red-400;
    }
}