/**
 * Sass CSS Framework: Buttons
 *
 * Requires: mizzou-variables.scss, mizzou-base.scss
 *
 * @author Josh Hughes (hughesjd@missouri.edu), Travis Cook (cooktw@missouri.edu), Undergraduate Studies, University of Missouri
 * @copyright 2018 Curators of the University of Missouri
 */

/**
 * Sets up a button with custom attributes.
 *
 * @param color $color-background Background color.
 * @param color $color-text Text color.
 * @param color $color-border Border color.
 * @param color $color-background-hover (Optional) Background color on hover and focus. Defaults to $color-background.
 * @param color $color-text-hover (Optional) Text color on hover and focus. Defaults to $color-text.
 * @param color $color-border-hover (Optional) Border color on hover and focus. Defaults to $color-border.
 * @param string $size (Optional) Text size. Can be 'small' or 'large'. Defaults to 'small'.
 */
 @mixin custom-button(
    $color-background,
    $color-text,
    $color-border,
    $color-background-hover:        $color-background,
    $color-text-hover:              $color-text,
    $color-border-hover:            $color-border,
    $size:                          'small'
)
{
    font-weight: $font-weight-bold;
    display: inline-block;
    text-decoration: none;
    font-family: $font-sans-serif;
    cursor: pointer;
    text-align: center;
    transition-property: background, border, color;
    transition-duration: 0.1s;
    transition-timing-function: linear;
    border-style: solid;
    border-width: 1px;
    background: $color-background;
    color: $color-text;
    border: 1px solid $color-border;
    border-radius: 2px;
    
    // Font size
    @if ($size == 'large') {
        font-size: 24px;
        padding: .5rem .875rem;
    } @else { // $size == 'small'
        font-size: 16px;
        padding: .3125rem .625rem;
    }
    
    @media print
    {
        background: #fff;
        border: 1px solid #000;
        color: #000;
    }
        
    &:hover,
    &:focus,
    &:active
    {
        background: $color-background-hover;
        color: $color-text-hover;
        border-color: $color-border-hover;
    }
}

/**
 * Sets up a button with common attributes.
 *
 * @param string $size (Optional) Text size. Can be 'small' or 'large'. Defaults to 'small'.
 * @param string $color (Optional) Button color. Can be 'brown', 'gold', 'grey', or 'admissions-red'. Defaults to 'brown'.
 * @param string $theme (Optional) Color theme. Can be 'light' or 'dark'. Dark gives the button a lighter border. Defaults to 'light'.
 * @param boolean $include-print-style (Optional) Whether to make the button black and white for printing. Defaults to true.
 */
@mixin button(
    $size:                      'large',
    $color:                     'brown',
    $theme:                     'light'
)
{
    @if ($theme == 'dark') {
        @if ($color == 'gold') {
            @include custom-button(
                $color-background: $color-mu-gold,
                $color-text: $color-grey-600,
                $color-border: transparent,
                $color-background-hover: $color-gold-200,
                $size: $size
            );
        } @else if ($color == 'grey') {
            @include custom-button(
                $color-background: $color-grey-200,
                $color-text: $color-grey-500,
                $color-border: transparent,
                $color-background-hover: $color-grey-100,
                $color-text-hover: #000,
                $size: $size
            );
        } @else if ($color == 'tan') {
            @include custom-button(
                $color-background: $color-gold-200,
                $color-text: $color-grey-600,
                $color-border: transparent,
                $color-background-hover: $color-mu-gold,
                $color-text-hover: #000,
                $size: $size
            );
        } @else if ($color == 'admissions-red') {
            @include custom-button(
                $color-background: $color-red-400,
                $color-text: #fff,
                $color-border: $color-red-400,
                $color-background-hover: $color-red-600,
                $color-text-hover: $color-gold-400,
                $color-border-hover: $color-red-400,
                $size: $size
            );
        } @else { // $color == 'brown'
            @include custom-button(
                $color-background: $color-brown-400,
                $color-text: #fff,
                $color-border: transparent,
                $color-background-hover: $color-brown-500,
                $size: $size
            );
        }
    } @else if $theme == 'ghost' {
        @if ($color == 'gold') {
            @include custom-button(
                $color-background: transparent,
                $color-text: #fff,
                $color-border: $color-mu-gold,
                $color-text-hover: $color-mu-gold,
                $color-border-hover: darken($color-mu-gold, 25%),
                $size: $size
            );
        } @else if ($color == 'white') {
            @include custom-button(
                $color-background: transparent,
                $color-text: #000,
                $color-border: #000,
                $color-background-hover: $color-grey-100,
                $color-text-hover: #000,
                $size: $size
            );
        }
    } @else { // $theme == 'light'
        @if ($color == 'gold') {
            @include custom-button(
                $color-background: $color-mu-gold,
                $color-text: $color-grey-600,
                $color-border: $color-gold-500,
                $color-background-hover: $color-gold-200,
                $size: $size
            );
        } @else if ($color == 'tan') {
            @include custom-button(
                $color-background: $color-gold-200,
                $color-text: $color-grey-600,
                $color-border: $color-gold-500,
                $color-background-hover: $color-mu-gold,
                $color-text-hover: #000,
                $size: $size
            );
        } @else if ($color == 'grey') {
            @include custom-button(
                $color-background: $color-grey-600,
                $color-text: #fff,
                $color-border: #000,
                $color-background-hover: $color-grey-600,
                $color-text-hover: $color-gold-300,
                $size: $size
            );
        } @else if ($color == 'white') {
            @include custom-button(
                $color-background: #fff,
                $color-text: $color-brown-500,
                $color-border: #000,
                $color-background-hover: $color-grey-100,
                $color-text-hover: #000,
                $size: $size
            );
        } @else if ($color == 'admissions-red') {
            @include custom-button(
                $color-background: $color-red-400,
                $color-text: #fff,
                $color-border: $color-red-500,
                $color-background-hover: $color-red-500,
                $color-text-hover: $color-gold-400,
                $color-border-hover: $color-red-600,
                $size: $size
            );
        } @else { // $color == 'brown'
            @include custom-button(
                $color-background: $color-brown-400,
                $color-text: #fff,
                $color-border: $color-brown-500,
                $color-background-hover: $color-brown-500,
                $color-border-hover: $color-brown-600,
                $size: $size
            );
        }
    }
}

/**
 * Sets up a red Admissions button with Truman included.
 *
 * @param string $theme (Optional) Color theme. Can be 'light' or 'dark'. Dark gives the button a lighter border. Defaults to 'light'.
 */
@mixin admissions-button-with-truman(
    $theme:                     'light'
)
{
    @include button(
        $size: 'large',
        $color: 'admissions-red',
        $theme: $theme
    );
    position: relative;
    margin-top: 48px;
    min-width: 204px;
    
    // Truman Hands
    &:after 
    { 
        content: '';
        display: block;
        width: 100%;
        height: 22px;
        background: transparent url('#{$image-path}/truman-paws.png') no-repeat center center;
        background-size: 140px 22px;
        position: absolute;
        top: -10px;
        left: 0px;
        
        @media #{$query-2x}
        {
        	background-image: url('#{$image-path}/truman-paws-2x.png');
        }
    }
    
    // Truman Head
    &:before
    { 
        content: '';
        display: block;
        width: 100%;
        height: 48px;
        background: transparent url('#{$image-path}/truman-face.png') no-repeat center 20px;
        background-size: 80px 83px;
        position: absolute;
        top: -49px;
        left: 0;
        transition: background linear .2s;
        
        @media #{$query-2x} 
        {
        	background-image: url('#{$image-path}/truman-face-2x.png');
        }
    }
    
    &:hover,
    &:focus,
    &:active
    {
        &:before 
        {
            background-position: center 0px;			
        }
    }
}

// Setup defaults
.miz-comp-button
{
    @include button(
        $color: 'gold',
        $theme: 'light',
        $size: 'large'
    );
}

.miz-comp-button--ghost
{
    @include button(
        $color: 'white',
        $theme: 'ghost'
    );

    &--gold
    {
        @include button(
            $color: 'gold',
            $theme: 'ghost'
        );
    }
}

.miz-comp-button--small
{
    @include button(
        $size: 'small',
        $color: 'gold'
    );
}

.miz-comp-button--secondary
{
    @include button(
        $theme: 'dark',
        $color: 'tan'
    );
}

.miz-comp-button--apply
{
    @include admissions-button-with-truman(
        $theme: 'dark'
    );
    display: block;
    
    @media #{$query-624}
    {
        position: relative;
        margin-top: 0;
    }

    @media #{$query-944}
    {
        @include admissions-button-with-truman(
            $theme: 'light'
        );
        display: block;
    }
}

.button
{
    @include button(
        $color: 'gold',
        $theme: 'light',
        $size: 'large'
    );

    &.button--ghost
    {
        @include button(
            $color: 'white',
            $theme: 'ghost'
        );

        &--gold
        {
            @include button(
                $color: 'gold',
                $theme: 'ghost'
            );

            &.button--small
            {
                @include button(
                    $size: 'small',
                    $color: 'gold',
                    $theme: 'ghost'
                );
            }
        }

        &.button--small
        {
            @include button(
                $size: 'small',
                $color: 'white',
                $theme: 'ghost'
            );
        }
    }

    &.button--small
    {
        @include button(
            $size: 'small',
            $color: 'gold'
        );
    }

    &.button--secondary
    {
        @include button(
            $theme: 'light',
            $color: 'tan'
        );

        &.button--small
        {
            @include button(
                $size: 'small',
                $color: 'tan',
                $theme: 'light'
            );
        }
    }

    &.button--apply
    {
        @include admissions-button-with-truman(
            $theme: 'dark'
        );
        display: block;
        
        @media #{$query-624}
        {
            position: relative;
            margin-top: 0;
        }

        @media #{$query-944}
        {
            @include admissions-button-with-truman(
                $theme: 'light'
            );
            display: block;
        }
    }
}