/**
 * Sass CSS Framework: Base
 *
 * Requires: mizzou-variables.scss
 *
 * @author Josh Hughes (hughesjd@missouri.edu), Travis Cook (cooktw@missouri.edu), Undergraduate Studies, University of Missouri
 * @copyright 2018 Curators of the University of Missouri
 */

/* @group Mixins */

/**
 * Image replacement technique. Offsets the element text and sets a background image.
 *
 * @param string $filename (Optional) Filename of the image. Defaults to 'none'.
 * @param unit $width (Optional) Width of the image. Defaults to 'none'.
 * @param unit $height (Optional) Height of the image. Defaults to 'none'.
 */
 @mixin replace-with-image(
    $filename:      'none',
    $width:         'none',
    $height:        'none'
)
{
    display: block;
    text-indent: 200%;
    white-space: nowrap;
    overflow: hidden;
    padding: 0;
    background-repeat: no-repeat;
    
    @if ($filename != 'none') {
        background-image: url('#{$image-path}/#{$filename}');
    }
    
    @if ($width != 'none') {
        width: $width;
    }
    
    @if ($height != 'none') {
        height: $height;
    }
}

/**
 * This method sets an image as content in a pseudo element. This allows the image to be printed reliably.
 *
 * @param string $filename Filename of the image.
 * @param unit $width (Optional) Width of the image. Defaults to 'none'.
 * @param unit $height (Optional) Height of the image. Defaults to 'none'.
 */
@mixin print-image(
    $filename,
    $width:         'none',
    $height:        'none'
)
{
    @include replace-with-image;
    position: relative;
    background: none;
    
    @if ($width != 'none') {
        width: $width;
    }
    
    @if ($height != 'none') {
        height: $height;
    }
    
    &:after
    {
        content: url('#{$image-path}/#{$filename}');
        text-indent: 0;
        display: block;
        position: absolute;
        top: 0;
        left: 0;
        
        @if ($width != 'none') {
            width: $width;
        }
        
        @if ($height != 'none') {
            height: $height;
        }
    }   
}

/**
 * Apply on a container to have that container enclose floated elements.
 * @see http://cssmojo.com/the-very-latest-clearfix-reloaded/
 */
@mixin clear-fix()
{
    &:after
    {
        content: ' ';
        display: block;
        clear: both;
    }
}

/**
 * Sets a standard margin for every element, except for the last child of the container.
 *
 * @param unit $margin-bottom Bottom margin. Defaults to 16px.
 */
@mixin standard-margin(
    $margin-bottom:     16px
)
{
    margin: 0 0 #{$margin-bottom} 0;
    
    &:last-child
    {
        margin-bottom: 0;
    }
}

/**
 * Center align an image or figure.
 */
@mixin align-center()
{
    margin: 0 auto 16px auto;
    text-align: center;
    display: block;
}

/**
 * Left align an image or figure.
 */
@mixin align-left()
{
    &,
    &:last-child
    {
        margin: 0 16px 16px 0;
        float: left;
        display: block;
    }
}

/**
 * Right align an image or figure.
 */ 
@mixin align-right()
{
    &,
    &:last-child
    {
        margin: 0 0 16px 16px;
        float: right;
        display: block;
    }
}

/**
 * Ignore set widths and heights for images, so they work well with responsive containers.
 */ 
@mixin flexible-image()
{
    max-width: 100%;
    width: auto;
    height: auto;
}

/* @end */

/* @group Viewport */

// This is primarily to deal with this issue:
// http://timkadlec.com/2012/10/ie10-snap-mode-and-responsive-design/
@-ms-viewport
{
    width: $viewport-width;
}

@-o-viewport
{
    width: $viewport-width;
}

@viewport
{
    width: $viewport-width;
}

/* @end */

/* @group General Settings / Major Containers */

*,
*:before,
*:after
{
    box-sizing: border-box;
}

// Generally the target of a "Skip to Content" link
*[tabindex="-1"] 
{
    outline: none;
}

article,
aside,
audio,
canvas,
details,
figcaption,
figure,
footer,
header,
hgroup,
main,
menu,
nav,
section,
video
{
    display: block;
    margin: 0;
    padding: 0;
}

// @see https://adactio.com/journal/10019
a,
button,
input,
select,
textarea,
label,
summary
{
    touch-action: manipulation;
}

html
{
    -moz-osx-font-smoothing: grayscale;
    -webkit-font-smoothing: antialiased;
    -webkit-text-size-adjust: 100%;
    font-family: $font-sans-serif;
    font-size: 16px;
    line-height: 1.5;
    min-width: 320px;
    
    @media print
    {
        background: #fff;
    }
}

body
{
    background: #fff;
    color: $color-grey-500;
    margin: 0;
    
    @media print
    {
        padding: 16px;
        background: #fff;
    }
}

nav
{
    @media print
    {
        display: none;
    }
}

/* @end */

/* @group Text Format */

b,
strong,
.bold
{
    font-weight: $font-weight-bold;
}

i,
em,
.italic
{
    font-style: italic;
    
    em
    {
        font-style: normal;
    }
}

u
{
    text-decoration: none; // Sorry, unacceptable to use this
}

.no-bold
{
    font-weight: normal;
}

.no-italic
{
    font-style: normal;
}

.no-line-break
{
    white-space: nowrap;
}

.small-caps
{
	font-size: 0.9em;
	text-transform: uppercase;
}

.hanging-double-quote
{
	text-indent: -.3em;
}

.hanging-quote
{
	text-indent: -.2em;
}

/* @end */

/* @group Headings and Paragraphs */

h1,
h2,
h3,
h4,
h5,
h6,
.like-h1,
.like-h2,
.like-h3,
.like-h4,
.like-h5,
.like-h6
{
    @include standard-margin;
    font-weight: $font-weight-bold;
    color: #000;
    
    a
    {
        text-decoration: none;
    }
}

h1,
.like-h1
{
    font-size: 30px;
    line-height: 1.2;
    
    @if ($query-larger-text != false) {
        @media #{$query-larger-text}
        {
            font-size: 48px;
            font-weight: 300; // Light for Proxima Nova
        }
    }
}

h2,
.like-h2
{
    font-size: 21px;
    line-height: 1.4;
    
    @if ($query-larger-text != false) {
        @media #{$query-larger-text}
        {
            font-size: 32px;
            line-height: 1.2;
        }
    }
}

h3,
.like-h3
{
    font-size: 18px;
    line-height: 1.4;
    
    @if ($query-larger-text != false) {
        @media #{$query-larger-text}
        {
            font-size: 24px;
        }
    }
}

h4,
.like-h4
{
    font-size: 16px;
    
    @if ($query-larger-text != false) {
        @media #{$query-larger-text}
        {
            font-size: 20px;
        }
    }
}

h5,
h6,
.like-h5,
.like-h6
{
    font-size: 16px;
}

h6,
.like-h6
{
    color: $color-grey-500;
}

.heading--with-separator
{
    font-weight: $font-weight-bold;
    text-transform: uppercase;
    padding-top: 4px;
    padding-bottom: 4px;
    border-bottom: 1px solid $color-grey-200;
    
    @if ($query-larger-text != false) {
        @media #{$query-larger-text}
        {
            font-weight: $font-weight-bold;
        }
    }
    
    span
    {
        font-weight: normal;
        text-transform: none;
    }
}

.miz-main-content__header
{
    margin-bottom: 1rem;
    line-height: 1;
}

p
{
    @include standard-margin;
}

/* @end */

/* @group Images, Figures, and Video */

img
{
    border: 0;
    
    @if ($use-flexible-images == true) {
        @include flexible-image;
    }
}

figure,
.alignnone,
.alignleft,
.aligncenter,
.alignright
{
    @include standard-margin;
    max-width: 100%;
    
    img
    {
        display: block;
    }
}

figcaption,
.like-figcaption
{
    font-weight: $font-weight-bold;
    font-size: 14px;
    padding-top: 8px;
    
    p
    {
        font-size: inherit;
    }
}

figure.left,
.alignleft
{
    @if ($query-inset-images != false) {        
        @media #{$query-inset-images}
        {
            @include align-left;
            max-width: $inset-image-width;
        }
    }
}

figure.center,
.aligncenter
{
    @include align-center;
}

figure.right,
.alignright
{
    @if ($query-inset-images != false) {        
        @media #{$query-inset-images}
        {
            @include align-right;
            max-width: $inset-image-width;
        }
    }
}

iframe
{
    border: 0;
    clear: both;
    
    @if ($use-flexible-images == true) {
        @include flexible-image;
    }
}

.video
{
    // Defaults to widescreen
    @include standard-margin;
    position: relative;
    padding-bottom: 56.25%; // 16/9 aspect ratio
    height: 0;
    overflow: hidden;
    
    &.video--4x3
    {
        padding-bottom: 75%; // 4/3 aspect ration
    }
    
    iframe
    {
        position: absolute;
        top:0;
        left: 0;
        width: 100%;
        height: 100%;
    }
}

/* @end */

/* @group Blockquotes */
    
blockquote,
.like-blockquote
{
    @include standard-margin;
    font-size: 18px;
    font-weight: $font-weight-bold;
    border-left: 4px solid $color-gold-400;
    background: #fff;
    color: #000;
    padding: 16px;
        
    @if ($query-larger-text != false) {
        @media #{$query-larger-text}
        {
            font-size: 24px;
            padding: 16px 32px;
        }
    }
    
    p 
    {
        font-size: inherit;
        margin: 0;
    }
}

.blockquote__attribution
{
    margin-top: 8px;
    text-align: right;
    font-size: 16px;
    font-weight: normal;
    color: $color-grey-500;
}

/* @end */

/* @group Addresses */

address
{
    font-style: normal;
}

/* @end */

/* @group Horizontal Rules */

hr
{
    @include standard-margin;
    clear: both;
    border: 0;
    border-bottom: 1px solid $color-grey-200;
    
    &.hr--dashed
    {
        border-style: dashed;
    }
}

/* @end */

/* @group Lists */

ul,
ol
{
    @include standard-margin;
    
    ul,
    ol
    {
        margin-bottom: 0;
    }
}

li
{
    li,
    p
    {
        font-size: inherit;
    }
}

.list--no-style
{
    list-style: none;
    padding: 0;
}

.list--spaced
{
    > li
    {
        @include standard-margin;
    }
}

.list--square
{
    list-style: square;
}

.list--disc
{
    list-style: disc;
}

.list--circle
{
    list-style: circle;
}

.list--decimal
{
    list-style: decimal;
}

.list--lower-alpha
{
    list-style: lower-alpha;
}

.list--upper-alpha
{
    list-style: upper-alpha;
}

.list--lower-roman
{
    list-style: lower-roman;
}

.list--upper-roman
{
    list-style: upper-roman;
}

dl,
dd
{
    @include standard-margin;
}

dt
{
    font-weight: $font-weight-bold;
}

/* @end */

/* @group Forms */

form
{
    @include standard-margin;
}

fieldset
{
    @include standard-margin(
        $margin-bottom: 32px
    );
}

fieldset,
legend
{
    padding: 0;
    border: 0;
}

legend
{
    @extend .like-h2;
}

input[type='text'],
input[type='password'],
input[type='search'],
input[type='tel'],
input[type='url'],
input[type='email'],
input[type='date'],
textarea
{
    margin: 0;
    padding: 4px;
    border: 1px solid $color-grey-200;
    font-size: 100%;
    font-family: inherit;
    color: $color-grey-500;
    border-radius: 0;
    box-shadow: inset 0 0 4px transparentize($color-grey-200, 0.25);
    display: block;
    -webkit-appearance: none;
    width: 100%;
    
    &:focus
    {
        border: 1px solid $color-gold-500;
        outline: 4px solid transparentize($color-gold-500, 0.25);
        outline-offset: 0;
    }
}

select,
input[type='file']
{
    color: $color-grey-500;
    font-family: inherit;
    font-size: inherit;
}

input[type='submit'],
input[type='reset'],
input[type='button'],
button
{
    font-family: inherit;
    font-size: inherit;
    vertical-align: top;
    cursor: pointer;
    
    &::-moz-focus-inner
    {
        border: 0;
        padding: 0;
    }
}

/* @end */

/* @group Tables */

table
{
    @include standard-margin;
    border-collapse: collapse;
    border-spacing: 0;
}

th,
td
{
    border: 1px solid $color-grey-200;
    text-align: left;
}

th,
td
{
    vertical-align: top;
    padding: 4px 8px;
}

th[scope='col']
{
    border-bottom-width: 2px;
}

th[scope='row']
{
    border-right-width: 2px;
}

caption
{
    @extend .like-h4;
    margin: 0 0 8px 0;
    text-align: left;
}

/* @end */

/* @group Code and Preformatted Text */

pre,
code,
var
{
    font-family: $font-monospace;
    font-size: 14px;
    white-space: pre-wrap;
}

var
{
    font-weight: bold;
    font-style: normal;
}

/* @end */

/* @group Links */

a
{
    color: #111111;
    text-decoration: underline;
    
    &:hover,
    &:focus,
    &:active
    {
        color: $color-gold-500;
        text-decoration: none;
    }
}

/* @end */

/* @group Marks and Notations */

abbr,
acronym
{
    border-bottom: 1px dotted $color-grey-200;
    cursor: help;
    text-decoration: none;
}

sub,
sup 
{
    position: relative;
    font-size: 0.75em;
    line-height: 0;
    vertical-align: baseline;
}

sup 
{
    top: -0.5em;
}

sub 
{
    bottom: -0.25em;
}

mark
{
    background: $color-gold-200;
}

/* @end */

/* @group Margins */

.no-margin
{
    margin: 0;
}

.margin,
.margin:last-child
{
    margin-bottom: 16px;
}

.margin-2x,
.margin-2x:last-child
{
    margin-bottom: 32px;
}

/* @end */

/* @group Indentation */

.indent
{
    padding-left: 16px;
}

.indent-2x
{
    padding-left: 32px;
}

.hanging-indent
{
    padding-left: 16px;
    text-indent: -16px;
}

.hanging-indent-2x
{
    padding-left: 32px;
    text-indent: -32px;
}

/* @end */

/* @group Visibility */

.hidden
{
    display: none;
}

.hidden-from-sight
{
    // Hat tip to the Lightning Design System: https://www.lightningdesignsystem.com/components/utilities/visibility/
    position: absolute;
    margin: -1px;
    border: 0;
    padding: 0;
    width: 1px;
    height: 1px;
    overflow: hidden;
    clip: rect(0 0 0 0);
}

/* @end */

/* @group Editing */

.edit-link
{
    font-weight: $font-weight-bold;
}

/* @end */

/* @group Accordion */

.accordion
{
    .card
    {
        .card-header
        {
            background-color: $color-mu-gold;
            color: $color-black;

            &:hover, &:active, &:focus
            {
                cursor: pointer;
                background-color: lighten($color-mu-gold, 10%);
            }
        }
    }
}

/* @end */