/**
 * Setup a hamburger-style menu button
 *
 * Usage:
 *
 * setupMobileMenuButton('nav');
 * setupMobileMenuButton('#additional-menu', '#mobile-menu', 'Show Menu');
 *
 * @author Josh Hughes (hughesjd@missouri.edu), Undergraduate Studies, University of Missouri
 * @copyright 2016 Curators of the University of Missouri
 */

// Class manipulation object
var objClassManipulation = new ClassManipulation();

/**
 * Toggle visibility of menu
 *
 * @param object objEvent Event
 */
function toggleMenuVisibility(objEvent)
{
    objEvent.preventDefault();
    
    // Target two levels up
    objClassManipulation.toggleClass(domTraversal('parent', domTraversal('parent', this)), 'nav--hide');
    return false;
}

/**
 * Adds the menu button element and sets up the toggle effect
 *
 * @param string strSelector Selector to apply to (i.e. 'nav' or '.menu')
 * @param string strCloneToSelector (Optional) Selector to clone the nav to
 * @param string strMenuTitle (Optional) Text node for the menu button. Defaults to 'Menu'
 */
function setupMobileMenuButton(strSelector, strCloneToSelector, strMenuTitle)
{
    // Setup variables
    var objClonedElement, objElements, objElementsToCloneTo, objHTMLTag, objLinks, objMobileMenuButton, objMobileMenuButtonLink, objMobileMenuButtonHeader;
    
    // Set default for menu title
    if (typeof strMenuTitle === 'undefined') {
        strMenuTitle = 'Menu';
    }
    
    // Grab matching elements
    objElements = document.querySelectorAll(strSelector);
    
    if (objElements.length > 0) {
        for (var i = 0; i < objElements.length; i++) {
            
            // Clone nav
            if ((typeof strCloneToSelector !== 'undefined') && (strCloneToSelector !== false)) {
                objElementsToCloneTo = document.querySelectorAll(strCloneToSelector);
    
                if (objElementsToCloneTo.length > 0) {
                    objClonedElement = objElements[i].cloneNode(true);
                    
                    // Remove id from cloned element
                    objClonedElement.removeAttribute('id');
                    
                    for (var intClones = 0; intClones < objElementsToCloneTo.length; intClones++) {
                        objElementsToCloneTo[intClones].appendChild(objClonedElement);
                    }
                }
            }
            
            // Create the following block:
            // <div class="nav__mobile-menu-button">
            //     <a href="#" class="nav__mobile-menu-button-link" tabindex="0">
            //         <span class="nav__mobile-menu-button-header">Menu</span>
            //     </a>
            // </div>
            objMobileMenuButton = document.createElement('div');
            objClassManipulation.addClass(objMobileMenuButton, 'nav__mobile-menu-button');
            
            objMobileMenuButtonLink = document.createElement('a');
            objMobileMenuButtonLink.href = '#';
            objMobileMenuButtonLink.tabIndex = 0;
            objClassManipulation.addClass(objMobileMenuButtonLink, 'nav__mobile-menu-button-link');
            
            objMobileMenuButtonHeader = document.createElement('span');
            objClassManipulation.addClass(objMobileMenuButtonHeader, 'nav__mobile-menu-button-header');
            
            objMobileMenuButtonHeader.appendChild(document.createTextNode(strMenuTitle));
            objMobileMenuButtonLink.appendChild(objMobileMenuButtonHeader);
            objMobileMenuButton.appendChild(objMobileMenuButtonLink);
            
            // Set click toggle
            objMobileMenuButtonLink.addEventListener('click', toggleMenuVisibility);
            
            // Insert the element
            objElements[i].insertBefore(objMobileMenuButton, objElements[i].firstChild);
            
            // Add a class to the element hiding the menu (if the element doesn't have an "nav--open-by-default" class
            if (!objClassManipulation.hasClass(objElements[i], 'nav--open-by-default')) {
                objClassManipulation.addClass(objElements[i], 'nav--hide');
            }
        }
        
        // Add 'has-menu-button' class to <html> tag
        objHTMLTag = document.getElementsByTagName('html');
        if (objHTMLTag.length > 0) {
            objClassManipulation.addClass(objHTMLTag[0], 'has-mobile-menu-button');
        }
    }
}