/**
 * Google Analytics Configuration
 *
 * @author Josh Hughes (hughesjd@missouri.edu), Undergraduate Studies, University of Missouri
 * @copyright 2016 Curators of the University of Missouri
 */
 
var objGoogleAnalyticsConfig = {
    code: 'XXXXXX',
    domain: 'XXXXXX.missouri.edu'
};