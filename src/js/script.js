/**
 * JavaScript for PROJECT
 *
 * @author Travis Cook (cooktw@missouri.edu), Undergraduate Studies, University of Missouri
 * @copyright 2018 Curators of the University of Missouri
 */

// Setup mobile menu button for main navigation
setupMobileMenuButton('.nav--primary-navigation', '#mobile-nav');