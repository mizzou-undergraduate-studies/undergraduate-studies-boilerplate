#!/bin/bash

# Backup WordPress Uploads

# Target directory
target=$(cd "$(dirname "${BASH_SOURCE[0]}")/../../" && pwd)

echo -e "Backing Up: Teaching for Learning Center with User 'muwhtlc@col.missouri.edu' on Server 'wh-prod-01-mgmt.missouri.edu' (will prompt for password)"
rsync -a --delete muwhtlc@col.missouri.edu@wh-prod-01-mgmt.missouri.edu:/var/www/html/tlc.missouri.edu/www/wp-content/uploads/ "$target/app/public/wp-content/uploads"

# Change to the target directory
cd "$target"