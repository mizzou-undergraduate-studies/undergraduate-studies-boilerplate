#!/bin/bash

# Backup WordPress Uploads

# Target directory
target=$(cd "$(dirname "${BASH_SOURCE[0]}")/../../" && pwd)

# Backup the Production database
echo -e "\n\nBacking Up: Remote MySQL Database with User 'cooktw' (will prompt for password) in Production"
mysql -h mysql-web1.missouri.edu muvpugsstudies_1 -u cooktw -p -N -e 'show tables like "PREFIX\_%"' | xargs mysqldump -h mysql-web1.missouri.edu muvpugsstudies_1 -u cooktw -p > "$target/src/sql/prod.sql"


# Backup the Development database
echo -e "\n\nBacking Up: Remote MySQL Database with User 'cooktw' (will prompt for password) in Development"
mysql -h mysql-webdev.missouri.edu muvpugsstudies_1 -u cooktw -p -N -e 'show tables like "PREFIX\_%"' | xargs mysqldump -h mysql-webdev.missouri.edu muvpugsstudies_1 -u cooktw -p > "$target/src/sql/dev.sql"

# Change to the target directory
cd "$target"