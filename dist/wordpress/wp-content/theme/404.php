<?php
/**
 * 404 Errors
 *
 * @author Josh Hughes (hughesjd@missouri.edu), Undergraduate Studies, University of Missouri
 * @copyright 2016 Curators of the University of Missouri
 */

// Setup Timber
$aryContext = Timber::get_context();

// Set page title
$aryContext['page']['title'] = 'Page could not be found';

// Render view
Timber::render(array('site-404.twig', '404.twig'), $aryContext);