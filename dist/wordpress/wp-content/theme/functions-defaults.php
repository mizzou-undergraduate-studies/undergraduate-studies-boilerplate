<?php
/**
 * Default Functions and Definitions
 *
 * @author Josh Hughes (hughesjd@missouri.edu), Travis Cook (cooktw@missouri.edu), Undergraduate Studies, University of Missouri
 * @copyright 2016 Curators of the University of Missouri
 */

// Timber is required: http://upstatement.com/timber
if (!class_exists('Timber')) {
    add_action('admin_notices', function()
    {
        printf('<div class="error"><p>Timber not activated. Make sure you <a href="%s">activate the plugin</a>.</p></div>', esc_url(admin_url('plugins.php#timber')));
    });
    return;
}

class StandardMizzouSite extends TimberSite
{
    public function __construct()
    {
        // Adjust template location
        Timber::$dirname = array('twig');

        // Remove unnecessary junk from wp_head
        remove_action('wp_head', 'rsd_link');
        remove_action('wp_head', 'wlwmanifest_link');
        remove_action('wp_head', 'wp_generator');
        remove_action('wp_head', 'rel_canonical');
        remove_action('wp_head', 'adjacent_posts_rel_link_wp_head');
        remove_action('wp_head', 'feed_links', 2);
        remove_action('wp_head', 'feed_links_extra', 3);
        remove_action('wp_head', 'wp_shortlink_wp_head');
        remove_action('wp_head', 'rest_output_link_wp_head');

        // Add HTML5 support for gallery and caption shortcodes
        add_theme_support('html5', array('gallery', 'caption'));

        // Add menu support
        add_theme_support('menus');

        // Add post thumbnail support and set size
        add_theme_support('post-thumbnails');

        // Allow editors to edit menus
        add_action('admin_init', array($this, 'allowEditorsToEditMenus'));

        // Twig additions
        add_filter('get_twig', array($this, 'twigAdditions'));

        // Modify Tiny_MCE init
        add_filter('tiny_mce_before_init', array($this, 'customFormatTinyMCE'));
        add_filter('acf/fields/wysiwyg/toolbars', array($this, 'acfCustomFormatTinyMCE'));

        // Remove emoji support
        add_action('init', array($this, 'disableEmoji'));
        add_filter('tiny_mce_plugins', array($this, 'disableTinyMCEEmoji'));

        // Setup shortcodes
        add_shortcode('home_url', array($this, 'homeURLShortCode'));
        add_shortcode('contentblock', array($this, 'contentBlockShortCode'));

        // Add SVG to mime types
        add_filter('upload_mimes', array($this, 'customMimeTypes'));

        // Disable XML-RPC
        add_filter('xmlrpc_enabled', '__return_false');

        // Prevent remote attackers from enumerating user names
        add_filter('redirect_canonical', array($this, 'scanForEnumerationAttempt'), 10, 2);

        // Continue on
        parent::__construct();
    }

    /**
     * Setup configuration variables for site
     *
     * @param array $aryContext Parent context variable
     * @return Updated $aryContext
     */
    public function siteConfiguration($aryContext)
    {
        // Map existing Timber options to aliases
        $aryContext['site']->asset_url = $aryContext['site']->theme->link . '/';
        $aryContext['site']->base_url = $aryContext['site']->url . '/';
        $aryContext['title'] = TimberHelper::get_wp_title();

        // Continue on
        return $aryContext;
    }

    /**
     * Get menu title and items
     *
     * @param string $strMenuName Name of menu
     * @return array Associative array with the following properties:
     *      - (string) name Name of the menu or empty string
     *      - (array) items Collection of TimberMenuItem objects or empty array
     */
    public static function getMenu($strMenuName)
    {
        $strMenuTitle = '';
        $aryMenuItems = array();

        if (is_nav_menu($strMenuName)) {
            $objMenu = new TimberMenu($strMenuName);
            $strMenuTitle = $objMenu->title;
            $aryMenuItems = $objMenu->get_items();
        }
        
        return array(
            'name'  => $strMenuTitle,
            'items' => $aryMenuItems
        );
    }

    /**
     * Get menu title and items from Menu Location setting
     *
     * @param string $strMenuName Name of menu
     * @return array Associative array with the following properties:
     *      - (string) name Name of the menu or empty string
     *      - (array) items Collection of TimberMenuItem objects or empty array
     */
    public static function getMenuLoc($strMenuName)
    {
        $strMenuTitle = '';
        $aryMenuItems = array();

        if (has_nav_menu($strMenuName)) {
            $objMenu = new TimberMenu($strMenuName);
            $strMenuTitle = $objMenu->title;
            $aryMenuItems = $objMenu->get_items();
        }

        return array(
            'name'  => $strMenuTitle,
            'items' => $aryMenuItems
        );
    }

    /**
     * Adds functions or extensions to Twig
     *
     * @param object $objTwig Existing Twig object
     * @return object Updated Twig object
     */
    public function twigAdditions($objTwig)
    {
        $objTwig->addExtension(new Twig_Extension_StringLoader());
        $objTwig->addFilter('date_ap_style', new Twig_Filter_Function(array($this, 'dateToAPStyle')));
        $objTwig->addFilter('menu_flatten_hierarchy', new Twig_Filter_Function(array($this, 'menuFlattenHierarchy')));
        $objTwig->addFilter('menu_item_link_list', new Twig_Filter_Function(array($this, 'menuItemLinkList')));
        $objTwig->addFilter('menu_previous_next_page', new Twig_Filter_Function(array($this, 'menuPreviousAndNextPage')));
        $objTwig->addFilter('menu_url_in_link_list', new Twig_Filter_Function(array($this, 'menuUrlInLinkList')));
        $objTwig->addFilter('htmlentities', new Twig_SimpleFilter('htmlentities', 'htmlentities', array('is_safe' => array('html'))));
        return $objTwig;
    }

    /**
     * Creates an AP-style date
     *
     * @param string $strDate Date string
     * @return string Date in AP format
     */
    public function dateToAPStyle($strDate)
    {
        $strAPStyleDate = '';
        $strTimestamp = strtotime($strDate);
        if ($strTimestamp !== false) {
            $strAPStyleDate = date('M. j, Y', $strTimestamp);

            // Fix months
            $strAPStyleDate = str_replace('Mar.', 'March', $strAPStyleDate);
            $strAPStyleDate = str_replace('Apr.', 'April', $strAPStyleDate);
            $strAPStyleDate = str_replace('May.', 'May', $strAPStyleDate);
            $strAPStyleDate = str_replace('Jun.', 'June', $strAPStyleDate);
            $strAPStyleDate = str_replace('Jul.', 'July', $strAPStyleDate);
        }

        return $strAPStyleDate;
    }

    /**
     * Flattens a nav item array so it isn't hierarchical
     *
     * @param array $aryMenu Associative array of menu items (TimberMenu objects)
     * @param boolean $boolIncludeChildLinks (Optional) Whether to recursively search child links. Defaults to true
     * @return array Menu items without hierarchy
     */
    public function menuFlattenHierarchy($aryMenu, $boolIncludeChildLinks = true)
    {
        $aryLinkList = array();

        if (!empty($aryMenu)) {
            foreach ($aryMenu as $objMenuItem) {
                $aryLinkList[] = $objMenuItem;

                // Pull in child links
                $aryChildren = $objMenuItem->children();
                if (($boolIncludeChildLinks === true) && (!empty($aryChildren))) {
                    $aryChildLinkList = $this->menuFlattenHierarchy($aryChildren);
                    $aryLinkList = array_merge($aryLinkList, $aryChildLinkList);
                }
            }
        }

        return $aryLinkList;
    }

    /**
     * Creates a list of URLs used by a menu link and any child links it might have, with infinite depth
     *
     * @param obj $objMenuItem Menu item
     * @return array List of URLs
     */
    public function menuItemLinkList($objMenuItem)
    {
        $aryLinkList = array();

        $strLink = $objMenuItem->link();
        if (trim($strLink) != '') {
            $aryLinkList[] = $strLink;

            // Recursively process child links
            $aryChildren = $objMenuItem->children();
            if (($aryChildren) && (!empty($aryChildren))) {
                foreach ($aryChildren as $objChildMenuItem) {
                    $aryLinkList = array_merge($aryLinkList, $this->menuItemLinkList($objChildMenuItem));
                }
            }
        }

        return array_unique($aryLinkList);
    }

    /**
     * Determine previous and next page in navigation
     *
     * @param array $arrayMenu Collection of TimberMenuItem objects
     * @param string $strCurrentPage Current page url. $aryMenu will be searched for this to determine the previous/next link
     * @param boolean $boolIncludeChildLinks (Optional) Whether to search child links too. Defaults to true
     * @return array With two keys, 'previous' and 'next', containing the menu item array for each
     */
    public function menuPreviousAndNextPage($aryMenu, $strCurrentPage, $boolIncludeChildLinks = true)
    {
        $aryPreviousMenuItem = $aryNextMenuItem = null;

        if (!empty($aryMenu)) {

            // Start by flattening the menu array so it's no longer hierarchical
            $aryLinkList = $this->menuFlattenHierarchy($aryMenu, $boolIncludeChildLinks);

            // Find the current page
            for ($i = 0; $i < count($aryLinkList); $i++) {

                $strLink = $aryLinkList[$i]->link();
                if ((trim($strLink) != '') && ($strLink == $strCurrentPage)) {
                    if (isset($aryLinkList[$i - 1])) {
                        $aryPreviousMenuItem = $aryLinkList[$i - 1];
                    }

                    if (isset($aryLinkList[$i + 1])) {
                        $aryNextMenuItem = $aryLinkList[$i + 1];
                    }
                    break;
                }
            }
        }

        return array(
            'previous'  => $aryPreviousMenuItem,
            'next'      => $aryNextMenuItem
        );
    }

    /**
     * Checks to see if a given URL is present in a menu
     *
     * @param obj $objMenu Menu
     * @param str $strUrl URL to check
     * @return boolean Whether or not the URL is in the menu
     */
    public function menuUrlInLinkList($aryMenu, $strUrl)
    {
        $aryLinkList = array();

        foreach ($aryMenu as $objMenuItem) {
            $aryLinkList = array_merge($aryLinkList, $this->menuItemLinkList($objMenuItem));
        }

        $aryLinkList = array_unique($aryLinkList);

        // Check to see if $strUrl is in the list
        return in_array($strUrl, $aryLinkList);
    }

    /*
     * Modifications to the TinyMCE editor
     */
    public function customFormatTinyMCE($aryTinyMce)
    {
        // Setup custom classes
        $aryCustomFormats = array(
            array(
                'title' => 'Alerts',
                'items' => array(
                    array(
                        'title'     => 'Error',
                        'selector'  => 'p',
                        'classes'   => 'alert alert-error'
                    ),
                    array(
                        'title'     => 'Success',
                        'selector'  => 'p',
                        'classes'   => 'alert alert-success'
                    ),
                    array(
                        'title'     => 'Danger',
                        'selector'  => 'p',
                        'classes'   => 'alert alert-danger'
                    ),
                    array(
                        'title'     => 'Warning',
                        'selector'  => 'p',
                        'classes'   => 'alert alert-warning'
                    ),
                    array(
                        'title'     => 'Information',
                        'selector'  => 'p',
                        'classes'   => 'alert alert-info'
                    ),
                    array(
                        'title'     => 'Light',
                        'selector'  => 'p',
                        'classes'   => 'alert alert-light'
                    ),
                    array(
                        'title'     => 'Dark',
                        'selector'  => 'p',
                        'classes'   => 'alert alert-dark'
                    ),
                )
            ),

            array(
                'title' => 'Buttons',
                'items' => array(
                    array(
                        'title'     => 'Small Button',
                        'selector'  => 'a',
                        'classes'   => 'button button--small'
                    ),
                    array(
                        'title'     => 'Large Button',
                        'selector'  => 'a',
                        'classes'   => 'button button--large'
                    ),
                    array(
                        'title'     => 'Secondary Button',
                        'selector'  => 'a',
                        'classes'   => 'button button--secondary'
                    ),
                    array(
                        'title'     => 'Ghost Button',
                        'selector'  => 'a',
                        'classes'   => 'button button--ghost'
                    ),
                    array(
                        'title'     => 'Gold Ghost Button',
                        'selector'  => 'a',
                        'classes'   => 'button button--ghost---gold'
                    ),
                )
            ),

            array(
                'title' => 'Heading Emulation',
                'items' => array(
                    array(
                        'title'     => 'Like Heading 1',
                        'selector'  => 'h2, h3, h4, h5, h6, p, li, th, td, div',
                        'classes'   => 'like-h1'
                    ),
                    array(
                        'title'     => 'Like Heading 2',
                        'selector'  => 'h1, h3, h4, h5, h6, p, li, th, td, div',
                        'classes'   => 'like-h2'
                    ),
                    array(
                        'title'     => 'Like Heading 3',
                        'selector'  => 'h1, h2, h4, h5, h6, p, li, th, td, div',
                        'classes'   => 'like-h3'
                    ),
                    array(
                        'title'     => 'Like Heading 4',
                        'selector'  => 'h1, h2, h3, h5, h6, p, li, th, td, div',
                        'classes'   => 'like-h4'
                    ),
                    array(
                        'title'     => 'Like Heading 5',
                        'selector'  => 'h1, h2, h3, h4, h6, p, li, th, td, div',
                        'classes'   => 'like-h5'
                    ),
                    array(
                        'title'     => 'Like Heading 6',
                        'selector'  => 'h1, h2, h3, h4, h5, p, li, th, td, div',
                        'classes'   => 'like-h6'
                    )
                )
            ),

            array(
                'title' => 'Horizontal Rules',
                'items' => array(
                    array(
                        'title'     => 'Dashed',
                        'selector'  => 'hr',
                        'classes'   => 'hr--dashed'
                    )
                )
            ),

            array(
                'title' => 'Indent',
                'items' => array(
                    array(
                        'title'     => 'Indent',
                        'selector'  => 'h1, h2, h3, h4, h5, h6, p, li, th, td, div',
                        'classes'   => 'indent'
                    ),
                    array(
                        'title'     => '2x Indent',
                        'selector'  => 'h1, h2, h3, h4, h5, h6, p, li, th, td, div',
                        'classes'   => 'indent-2x'
                    ),
                    array(
                        'title'     => 'Hanging Indent',
                        'selector'  => 'h1, h2, h3, h4, h5, h6, p, li, th, td, div',
                        'classes'   => 'hanging-indent'
                    ),
                    array(
                        'title'     => '2x Hanging Indent',
                        'selector'  => 'h1, h2, h3, h4, h5, h6, p, li, th, td, div',
                        'classes'   => 'hanging-indent-2x'
                    )
                )
            ),

            array(
                'title' => 'Lists',
                'items' => array(
                    array(
                        'title'     => 'Spaced',
                        'selector'  => 'ul, ol',
                        'classes'   => 'list--spaced'
                    ),
                    array(
                        'title'     => 'Unordered - Square',
                        'selector'  => 'ul',
                        'classes'   => 'list--square'
                    ),
                    array(
                        'title'     => 'Unordered - Disc',
                        'selector'  => 'ul',
                        'classes'   => 'list--disc'
                    ),
                    array(
                        'title'     => 'Ordered - Decimal (1.)',
                        'selector'  => 'ol',
                        'classes'   => 'list--decimal'
                    ),
                    array(
                        'title'     => 'Ordered - Lowercase Alpha (a.)',
                        'selector'  => 'ol',
                        'classes'   => 'list--lower-alpha'
                    ),
                    array(
                        'title'     => 'Ordered - Uppercase Alpha (A.)',
                        'selector'  => 'ol',
                        'classes'   => 'list--upper-alpha'
                    ),
                    array(
                        'title'     => 'Ordered - Lowercase Roman (i.)',
                        'selector'  => 'ol',
                        'classes'   => 'list--lower-roman'
                    ),
                    array(
                        'title'     => 'Ordered - Uppercase Roman (I.)',
                        'selector'  => 'ol',
                        'classes'   => 'list--upper-roman'
                    )
                )
            ),

            array(
                'title' => 'Margins',
                'items' => array(
                    array(
                        'title'     => 'No Margin',
                        'selector'  => 'h1, h2, h3, h4, h5, h6, p, li, th, td, div',
                        'classes'   => 'no-margin'
                    ),
                    array(
                        'title'     => 'Standard Margin',
                        'selector'  => 'h1, h2, h3, h4, h5, h6, p, li, th, td, div',
                        'classes'   => 'margin'
                    ),
                    array(
                        'title'     => '2x Margin',
                        'selector'  => 'h1, h2, h3, h4, h5, h6, p, li, th, td, div',
                        'classes'   => 'margin-2x'
                    )
                )
            ),

            array(
                'title' => 'Quotes',
                'items' => array(
                    array(
                        'title'     => 'Attribution',
                        'selector'  => 'p',
                        'classes'   => 'blockquote__attribution'
                    ),
                    array(
                        'title'     => 'Hanging Double Quote',
                        'selector'  => 'p',
                        'classes'   => 'hanging-double-quote'
                    ),
                    array(
                        'title'     => 'Hanging Single Quote',
                        'selector'  => 'p',
                        'classes'   => 'hanging-quote'
                    )
                )
            ),

            array(
                'title' => 'Text Styles',
                'items' => array(
                    array(
                        'title'     => 'Bold (Appearance Only)',
                        'inline'    => 'span',
                        'classes'   => 'bold'
                    ),
                    array(
                        'title'     => 'Italic (Appearance Only)',
                        'inline'    => 'span',
                        'classes'   => 'italic'
                    ),
                    array(
                        'title'     => 'No Word Wrap',
                        'inline'    => 'span',
                        'classes'   => 'no-break'
                    ),
                )
            )
        );

        // Prevents formats dropdown from emulating the style each item represents
        $aryTinyMce['preview_styles'] = 'font-family font-size font-weight text-decoration text-transform background-color color';

        // Insert styles, JSON encoded, into 'style_formats'
        $aryTinyMce['style_formats'] = json_encode($aryCustomFormats);

        // Fix block formats
        $aryTinyMce['block_formats'] = "Paragraph=p; Heading 1=h1; Heading 2=h2; Heading 3=h3; Heading 4=h4; Heading 5=h5; Heading 6=h6";

        // Customize toolbars
        $aryTinyMce['toolbar1'] = 'formatselect, styleselect, bold, italic, link, unlink, bullist, numlist, blockquote, hr, pastetext, removeformat, charmap, code, undo, redo, spellchecker, wp_fullscreen';
        $aryTinyMce['toolbar2'] = '';
        $aryTinyMce['wordpress_adv_hidden'] = false;

        // Add editor style
        $aryTinyMce['content_css'] = get_template_directory_uri() . '/css/editor-style.css';

        return $aryTinyMce;
    }

    /*
     * Enable modifications to the TinyMCE editor for ACF wysiwyg editors
     */
    public function acfCustomFormatTinyMCE($aryTinyMce)
    {
        // Customize toolbars
        $aryTinyMce['Full'][1] = array('formatselect', 'styleselect', 'bold', 'italic', 'link', 'unlink', 'bullist', 'numlist', 'blockquote', 'outdent', 'indent', 'hr', 'pastetext', 'removeformat', 'charmap', 'code', 'undo', 'redo', 'spellchecker', 'wp_fullscreen');
        $aryTinyMce['Full'][2] = array();

        return $aryTinyMce;
    }

    /*
     * Removes emoji support from Wordpress 4.2+
     */
    public function disableEmoji()
    {
        remove_action('wp_head', 'print_emoji_detection_script', 7);
        remove_action('admin_print_scripts', 'print_emoji_detection_script');
        remove_action('wp_print_styles', 'print_emoji_styles');
        remove_action('admin_print_styles', 'print_emoji_styles');
        remove_filter('the_content_feed', 'wp_staticize_emoji');
        remove_filter('comment_text_rss', 'wp_staticize_emoji');
        remove_filter('wp_mail', 'wp_staticize_emoji_for_email');
    }

    /*
     * Remove emoji from TinyMCE
     *
     * @param array $aryPlugins Plugins list
     * @return array Updated plugin list
     */
    function disableTinyMCEEmoji($aryPlugins)
    {
        return array_diff($aryPlugins, array('wpemoji'));
    }

    /**
     * Setup custom taxonomies
     */
    public function customTaxonomies()
    {
        // Setup Event Sponsor taxonomy
        register_taxonomy(
            'event_sponsor',
            'event',
            array(
                'labels'                    => array(
                    'name'                  => 'Event Sponsor',
                    'singular_name'         => 'Event Sponsor',
                    'all_items'             => 'All Event Sponsors',
                    'edit_item'             => 'Edit Event Sponsor',
                    'view_item'             => 'View Event Sponsor',
                    'update_item'           => 'Update Event Sponsor',
                    'add_new_item'          => 'Add New Event Sponsor',
                    'new_item_name'         => 'New Event Sponsor',
                    'parent_item'           => 'Parent Event Sponsor',
                    'parent_item_colon'     => 'Parent Event Sponsor:',
                    'search_items'          => 'Search Event Sponsors',
                    'not_found'             => 'No Event Sponsor found.'
                ),
                'hierarchical'      => true,
                'show_admin_column' => true,
                'show_ui'           => true,
                'query_var'         => true,
                'rewrite'           => true,
                'show_in_rest'      => true,
            )
        );
    }

    /**
     * Setup custom post types
     */
    public function customPostTypes()
    {
        // Content Block
        $aryContentBlockOptions = array(
            'labels' => array(
                'name'                  => __('Content Blocks'),
                'singular_name'         => __('Content Block'),
                'add_new'               => _x('Add New', 'block'),
                'add_new_item'          => __('Add New Content Block'),
                'edit_item'             => __('Edit Content Block'),
                'new_item'              => __('New Content Block'),
                'view_item'             => __('View Content Block'),
                'search_items'          => __('Search Content Blocks'),
                'not_found'             =>  __('No content blocks found'),
                'not_found_in_trash'    => __('No content blocks found in Trash'),
                'parent_item_colon'     => '',
                'menu_name'             => 'Content Blocks'
            ),
            'public'        => true,
            'has_archive'   => false,
            'supports'      => array('title', 'editor'),
            'menu_position' => 5,
            'menu_icon'     => 'dashicons-format-aside'
        );
        register_post_type('block', $aryContentBlockOptions);

        // Events
        $aryEventOptions = array(
            'labels' => array(
                'name'                  => __('Events'),
                'singular_name'         => __('Event'),
                'add_new'               => _x('Add New', 'event'),
                'add_new_item'          => __('Add New Event'),
                'edit_item'             => __('Edit Event'),
                'new_item'              => __('New Event'),
                'view_item'             => __('View Event'),
                'search_items'          => __('Search Events'),
                'not_found'             =>  __('No events found'),
                'not_found_in_trash'    => __('No events found in Trash'), 
                'parent_item_colon'     => '',
                'menu_name'             => 'Events'
            ),
            'public'        => true,
            'has_archive'   => false,
            'show_in_rest'  => true,
            'supports'      => array('title', 'editor', 'excerpt', 'thumbnail'),
            'menu_position' => 7,
            'menu_icon'     => 'dashicons-calendar-alt'
        );
        register_post_type('event', $aryEventOptions);

        add_filter('rest_prepare_event', function($response) {
            $response->data['acf'] = get_fields($response->data['id']);
            return $response;
       });
        
        // Staff
        $aryStaffOptions = array(
            'labels' => array(
                'name'                  => __('Staff'),
                'singular_name'         => __('Staff Member'),
                'add_new'               => _x('Add New', 'staff'),
                'add_new_item'          => __('Add New Staff Member'),
                'edit_item'             => __('Edit Staff Member'),
                'new_item'              => __('New Staff Member'),
                'view_item'             => __('View Staff Member'),
                'search_items'          => __('Search Staff Members'),
                'not_found'             =>  __('No staff members found'),
                'not_found_in_trash'    => __('No staff members found in Trash'), 
                'parent_item_colon'     => '',
                'menu_name'             => 'Staff'
            ),
            'public'        => true,
            'has_archive'   => false,
            'show_in_rest'  => true,
            'supports'      => array('title', 'editor', 'page-attributes', 'thumbnail'),
            'menu_position' => 9,
            'menu_icon'     => 'dashicons-groups'
        );
        register_post_type('staff', $aryStaffOptions);
    }

    /**
     * Outputs a content block
     *
     * @param string $mxdId Post id or slug
     */
    public static function contentBlock($mxdId)
    {
        // Pull the content block for the given id or slug
        $aryParams = array('post_type' => 'block');

        if (filter_var($mxdId, FILTER_VALIDATE_INT) === false) {
            $aryParams['name'] = $mxdId;
        } else {
            $aryParams['p'] = $mxdId;
        }

        $aryContext['page'] = Timber::get_post($aryParams);
        return Timber::compile(array('components/site-block.twig', 'components/block.twig'), $aryContext);
    }

    /**
     * Enables a shortcode for content blocks
     *
     * Usage:
     * [contentblock slug="list-of-programs"] // calls $this->contentBlock('list-of-programs')
     *
     * @param array $aryAttributes Array of attributes
     */
    public function contentBlockShortCode($aryAttributes)
    {
        $strContent = '';
        if (isset($aryAttributes['slug'])) {
            $strContent = $this->contentBlock($aryAttributes['slug']);
        }
        return $strContent;
    }

    /**
     * Enables a shortcode for the home URL
     *
     * Usage:
     * [home_url]
     *
     * @return string Home URL
     */
    public function homeURLShortCode()
    {
        return get_bloginfo('url');
    }

    /**
     * Add SVG to the list of acceptable mime-types
     *
     * @param array $aryMimeTypes Mime types
     * @return array Updated $aryMimeTypes
     */
    public function customMimeTypes($aryMimeTypes)
    {
        $aryMimeTypes['svg'] = 'image/svg+xml';
        return $aryMimeTypes;
    }

    /**
     * Prevent remote attackers from enumerating user names
     *
     * @param string $strRedirectionURL Redirection URL
     * @param string $strRequestedURL Requested URL
     * @return string (Possibly altered) Redirection URL
     */
    public function scanForEnumerationAttempt($strRedirectionURL, $strRequestedURL)
    {
        if (1 === preg_match('/\?author=([\d]*)/', $strRequestedURL)) {
            $strRedirectionURL = false;
        }

        return $strRedirectionURL;
    }

    /*
     * Allow editors to edit menus (+ other theme options / can't be helped)
     */
    public function allowEditorsToEditMenus()
    {
        $objEditorRole = get_role('editor');
        $objEditorRole->add_cap('edit_theme_options');
    }
}