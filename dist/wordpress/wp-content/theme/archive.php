<?php
/**
 * News Archive
 *
 * @author Travis Cook (hughesjd@missouri.edu), Undergraduate Studies, University of Missouri
 * @copyright 2018 Curators of the University of Missouri
 */

// Setup Timber
$aryContext = Timber::get_context();
$aryContext['page'] = new TimberTerm();

// Map existing Timber option for permalink to alias
$aryContext['page']->current_page = $aryContext['page']->link;

// Body class
$aryContext['page']->body_class = 'archive';

// Sub-navigation 
if ((isset($aryContext['page']->sub_navigation)) && ($aryContext['page']->sub_navigation !== false)) {
    $aryContext['page']->sub_navigation = MizzouSite::getMenu($aryContext['page']->sub_navigation);
}

// Get posts
$aryTaxParams[] = array(
    array(
        'taxonomy'  => $aryContext['page']->taxonomy,
        'field'     => 'slug',
        'terms'     => $aryContext['page']->slug
    ),
);
$aryParams = array(
    'posts_per_page'    => 15,
    'paged'             => $paged,
    'tax_query'         => $aryTaxParams,
);

$aryContext['news'] = Timber::get_posts($aryParams);

// Pagination
global $paged;
if (!isset($paged) || !$paged){
    $paged = 1;
}

$argsPagination = array(
    'mid_size'  => 1,
    'end_size'  => 1
);

query_posts($aryParams);
$aryContext['pagination'] = Timber::get_pagination($argsPagination);

// Render view
Timber::render('page-archive.twig', $aryContext);