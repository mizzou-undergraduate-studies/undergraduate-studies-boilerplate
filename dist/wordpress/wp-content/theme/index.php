<?php
/**
 * Homepage
 *
 * @author Josh Hughes (hughesjd@missouri.edu), Undergraduate Studies, University of Missouri
 * @copyright 2016 Curators of the University of Missouri
 */

// Setup Timber
$aryContext = Timber::get_context();

// Could use is_home(), but wanted to catch the search condition
$aryContext['page']['is_homepage'] = true;

// Set current page to base URL
$aryContext['page']['current_page'] = $aryContext['site']->base_url;

// Body class
$aryContext['page']['body_class'] = 'index';

// Feature story
$aryContext['feature'] = Timber::get_posts(array(
    'posts_per_page'    => 1,
    'category_name'      => 'featured'
));

// Setup standard news parameters
$aryNewsParams = array(
    'posts_per_page'    => 4,
    'category_name'      => 'news'
);

// Up the number of stories if no events
if (empty($aryContext['events'])) {
    $aryNewsParams['posts_per_page'] = 6;
}

// Get feature story id
if (isset($aryContext['feature'][0]->id)) {
    $aryNewsParams['post__not_in'] = array($aryContext['feature'][0]->id);
}

// News
$aryContext['news'] = Timber::get_posts($aryNewsParams);

// Render view
Timber::render(array('site-index.twig', 'index.twig'), $aryContext);