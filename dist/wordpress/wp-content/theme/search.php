<?php
/**
 * Search Results
 *
 * @author Josh Hughes (hughesjd@missouri.edu), Travis Cook (cooktw@missouri.edu), Undergraduate Studies, University of Missouri
 * @copyright 2016 Curators of the University of Missouri
 */

// Setup Timber
$aryContext = Timber::get_context();

// Body class
$aryContext['page']['body_class'] = 'search-archive';

// Set title
$aryContext['page']['title'] = 'Search Results';

if ((get_option('google_cse_id') == '') or (get_option('google_cse_id') == null)) {
    
    // Must have properly set variables
    if ((isset($aryContext['site']->search_field_name)) && (trim($aryContext['site']->search_field_name) != '') && (isset($aryContext['site']->hostname))) {
        $aryContext['search_results'] = mizzouGSAResults($aryContext['site']->search_field_name, $aryContext['site']->hostname);
    }
}

// Set search form value
if ((isset($aryContext['site']->search_field_name)) && (isset($_GET[$aryContext['site']->search_field_name]))) {
    $aryContext['search_form_value'] = $_GET[$aryContext['site']->search_field_name];
}

// Render view
Timber::render(array('site-search.twig', 'search.twig'), $aryContext);