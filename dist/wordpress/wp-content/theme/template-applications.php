<?php
/**
 * Template Name: Applications
 *
 * @author Travis Cook (cooktw@missouri.edu), Undergraduate Studies, University of Missouri
 * @copyright 2017 Curators of the University of Missouri
 */

// Setup Timber
$aryContext = Timber::get_context();
$aryContext['page'] = new TimberPost();

// Map existing Timber option for permalink to alias
$aryContext['page']->current_page = $aryContext['page']->link;

// Sub-navigation 
if ((isset($aryContext['page']->sub_navigation)) && ($aryContext['page']->sub_navigation !== false)) {
    $aryContext['page']->sub_navigation = MizzouSite::getMenu($aryContext['page']->sub_navigation);
}

// Render view
Timber::render('template-applications.twig', $aryContext);