<?php
/**
 * Theme Options
 *
 * @author Travis Cook (cooktw@missouri.edu), Undergraduate Studies, University of Missouri
 * @copyright 2018 Curators of the University of Missouri
 */


// create custom plugin settings menu
add_action('admin_menu', 'vpugs_create_menu');

function vpugs_create_menu() {

	//create new top-level menu
	add_theme_page('MU Site Settings', 'Site Settings', 'edit_theme_options', __FILE__, 'vpugs_settings_page' , plugins_url('/images/icon.png', __FILE__) );

	//call register settings function
	add_action( 'admin_init', 'register_vpugs_settings' );
}


function register_vpugs_settings() {
    // Standard Settings
    register_setting( 'vpugs-settings-group', 'google_analytics' );
    register_setting( 'vpugs-settings-group', 'google_search_console' );
    register_setting( 'vpugs-settings-group', 'publisher_name' );
    register_setting( 'vpugs-settings-group', 'publisher_url' );
    register_setting( 'vpugs-settings-group', 'subhead_name' );
    register_setting( 'vpugs-settings-group', 'subhead_url' );
    register_setting( 'vpugs-settings-group', 'typekit_id' );
    register_setting( 'vpugs-settings-group', 'google_cse_id' );

    // Social Media Settings
	register_setting( 'vpugs-settings-group', 'facebook_username' );
    register_setting( 'vpugs-settings-group', 'flickr_url' );
    register_setting( 'vpugs-settings-group', 'google_username' );
    register_setting( 'vpugs-settings-group', 'instagram_username' );
    register_setting( 'vpugs-settings-group', 'linkedin_username' );
    register_setting( 'vpugs-settings-group', 'pinterest_url' );
    register_setting( 'vpugs-settings-group', 'snapchat_username' );
    register_setting( 'vpugs-settings-group', 'twitter_url' );
	register_setting( 'vpugs-settings-group', 'youtube_url' );
    
}

function vpugs_settings_page() {
?>
<div class="wrap">
<h1>Site Settings</h1>

<form method="post" action="options.php">
    <?php settings_fields( 'vpugs-settings-group' ); ?>
    <?php do_settings_sections( 'vpugs-settings-group' ); ?>
    <table class="form-table">
        <tr valign="top">
            <th scope="row">Google Analytics Code</th>
            <td><input type="text" name="google_analytics" value="<?php echo esc_attr( get_option('google_analytics') ); ?>" /></td>
            
            <th>Google Search Console ID</th>
            <td><input type="text" name="google_search_console" value="<?php echo esc_attr( get_option('google_search_console') ); ?>" /></td>
        </tr>

        <tr valign="top">
            <th scope="row">Published By</th>
            <td><input type="text" name="publisher_name" value="<?php echo esc_attr( get_option('publisher_name') ); ?>" /></td>
            
            <th>Published By URL</th>
            <td><input type="text" name="publisher_url" value="<?php echo esc_attr( get_option('publisher_url') ); ?>" /></td>
        </tr>

        <tr valign="top">
            <th scope="row">Subhead Name</th>
            <td><input type="text" name="subhead_name" value="<?php echo esc_attr( get_option('subhead_name') ); ?>" /></td>
            
            <th>Subhead URL</th>
            <td><input type="text" name="subhead_url" value="<?php echo esc_attr( get_option('subhead_url') ); ?>" /></td>
        </tr>

        <tr valign="top">
            <th scope="row">Google CSE ID</th>
            <td><input type="text" name="google_cse_id" value="<?php echo esc_attr( get_option('google_cse_id') ); ?>" /></td>    
            
            <th scope="row">Typekit ID</th>
            <td><input type="text" name="typekit_id" value="<?php echo esc_attr( get_option('typekit_id') ); ?>" /></td>
        </tr>
    </table>

    <table class="form-table">
        <tr valign="top">
            <th colspan="4" style="border-bottom: 1px solid #ccc"><strong>Social Media</strong></th>
        </tr>
        <tr valign="top">
            <th scope="row">Facebook Username</th>
            <td><input type="text" name="facebook_username" value="<?php echo esc_attr( get_option('facebook_username') ); ?>" /></td>

            <th scope="row">Flickr URL</th>
            <td><input type="text" name="flickr_url" value="<?php echo esc_attr( get_option('flickr_url') ); ?>" /></td>
        </tr>
         
        <tr valign="top">
            <th scope="row">Google Username</th>
            <td><input type="text" name="google_username" value="<?php echo esc_attr( get_option('google_username') ); ?>" /></td>
            
            <th scope="row">Instagram Username</th>
            <td><input type="text" name="instagram_username" value="<?php echo esc_attr( get_option('instagram_username') ); ?>" /></td>
        </tr>

        <tr valign="top">
            <th scope="row">LinkedIn Username</th>
            <td><input type="text" name="linkedin_username" value="<?php echo esc_attr( get_option('linkedin_username') ); ?>" /></td>

            <th scope="row">Pinterest Username</th>
            <td><input type="text" name="pinterest_url" value="<?php echo esc_attr( get_option('pinterest_url') ); ?>" /></td>
        </tr>

        <tr valign="top">
            <th scope="row">Snapchat Username</th>
            <td><input type="text" name="snapchat_username" value="<?php echo esc_attr( get_option('snapchat_username') ); ?>" /></td>

            <th scope="row">Twitter Username</th>
            <td><input type="text" name="twitter_url" value="<?php echo esc_attr( get_option('twitter_url') ); ?>" /></td>
        </tr>

        <tr valign="top">
            <th scope="row">YouTube URL</th>
            <td><input type="text" name="youtube_url" value="<?php echo esc_attr( get_option('youtube_url') ); ?>" /></td>
        </tr>
    </table>
    
    <?php submit_button(); ?>

</form>
</div>
<?php } ?>