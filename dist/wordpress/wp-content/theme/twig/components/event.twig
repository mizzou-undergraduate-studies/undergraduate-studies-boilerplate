{#
/**
 * Event
 *
 * @author Travis Cook (cooktw@missouri.edu), Undergraduate Studies, University of Missouri
 * @copyright 2018 Curators of the University of Missouri
 *
 * Variables:
 * - event_attribution: (string) Attribution for the story; i.e. "From Undergraduate Studies", or "By Josh Murray" (optional).
 * - event_attribution_link: (string) Link for the attribution (optional).
 * - event_category: (string) Category for the story (optional).
 * - event_category_link: (string) Link for the category (optional).
 * - event_date: (string) Publication date (optional).
 * - event_excerpt: (string) Story excerpt.
 * - event_feature: (boolean) Whether or not this is a feature story (optional, see default).
 * - event_feature_hide_text: (boolean) Whether to hide the caption text for feature stories (optional).
 * - event_feature_text_alignment: (string) Alignment for feature story caption. Can be 'top left', 'top right', 'bottom left', or 'bottom right'. Defaults to 'bottom right' (optional).
 * - event_heading_level: (int) Heading level for the title. Can be 1-6 (optional, see default).
 * - event_image_alt: (string) Alt attribute for the story image (only required if image_src is set).
 * - event_image_src: (string) Src attribute for the story image (optional).
 * - event_image_resize_width: (int) Width to resize image to (optional, Timber-only).
 * - event_image_resize_height: (int) Height to resize image to (optional, Timber-only).
 * - event_link: (string) Link for the story.
 * - event_link_external: (string) External link for the story. Takes precedence over event_link (optional).
 * - event_location: (string) Event specific. Location of event (optional).
 * - event_title: (string) Title for the story.
 */
#}
{# Defaults #}
{% set event_feature = event_feature|default(false) %}
{% set event_heading_level = event_heading_level|default(3) %}

{# Fix heading level if it's not between 1 and 6 #}
{% if (event_heading_level > 6) or (event_heading_level <= 0) %}
    {% set event_heading_level = 3 %}
{% endif %}

{# Set link #}
{% if (event_link_external) and (event_link_external != 'http://') and (event_link_external != 'https://') %}
    {% set event_link = event_link_external %}
{% endif %}

<div class="event{% if (event_feature) %} event--feature{% endif %}">
    {% if (event_category) %}
    <p class="event__category">
        {% if (event_category_link) %}<a href="{{ event_category_link }}" aria-label="Category: {{ event_category }}">{% endif %}
            {{ event_category|striptags|raw }}
        {% if (event_category_link) %}</a>{% endif %}
    </p>
    {% endif %}
    
    {% if (event_image_src) and (event_image_alt) %}
    {# Apply resize filters #}
    {% if (event_image_resize_width) and (event_image_resize_height) %}
        {% set event_image_src = event_image_src|resize(event_image_resize_width, event_image_resize_height) %}
    {% elseif (event_image_resize_width) %}
        {% set event_image_src = event_image_src|resize(event_image_resize_width) %}
    {% endif %}
    
    <figure class="event__image">
        <a href="{{ event_link }}">
            <img src="{{ event_image_src }}" alt="{{ event_image_alt|striptags }}" />
        </a>
    </figure>
    {% endif %}
    
    {% if not (event_feature) or not (event_feature_hide_text) %}
    {% if (event_feature) %}<div class="event__text{% if (event_feature_text_alignment == 'top left') or (event_feature_text_alignment == 'top right') %} event__text--align-top{% endif %}{% if (event_feature_text_alignment == 'top left') or (event_feature_text_alignment == 'bottom left') %} event__text--align-left{% endif %}">{% endif %}

    {% if (event_end_date) %}
        {% if event_start_date %}
            {% if event_start_date|date('F') == event_end_date|date('F') %}
                {% set event_month = event_end_date|date('F') %}
            {% else %}
                {% set event_month = '<span aria-label="' ~ event_start_date|date('F') ~ '"/>' ~ event_start_date|date('M') ~ '</span>' ~ ' <span aria-hidden="true">&#8211;</span><span class="d-none">to</span> ' ~ '<span aria-label="' ~ event_end_date|date('F') ~ '"/>' ~ event_end_date|date('M') ~ '</span>'  %}
            {% endif %}

            {% if event_start_date|date('j') == event_end_date|date('j') %}
                {% set event_day = event_end_date|date('j') %}
            {% else %}
                {% set event_day = event_start_date|date('j') ~ '</span>' ~ ' <span aria-hidden="true">&#8211;</span><span class="d-none">to</span> ' ~ event_end_date|date('j') %}
            {% endif %}

            {% if event_start_date|date('Y') == event_end_date|date('Y') %}
                {% set event_year = event_end_date|date('Y') %}
            {% else %}
                {% set event_year = event_start_date|date('Y') ~ '</span>' ~ ' <span aria-hidden="true">&#8211;</span><span class="d-none">to</span> ' ~ event_end_date|date('Y') %}
            {% endif %}
        {% else %}
            {% set event_month = event_end_date|date('F') %}
            {% set event_day = event_end_date|date('j') %}
            {% set event_year = event_end_date|date('Y') %}
        {% endif %}
    <div class="event__date calendar-view">
        <p class="event__date--month">{{ event_month }}</p>
        <p class="event__date--day">{{ event_day }}</p>
        <p class="event__date--year">{{ event_year }}</p>
    </div>
    {% endif %}

    <div class="event__description">
        <h{{ event_heading_level }} class="event__title">
            <a href="{{ event_link }}" class="stretched-link">
                {{ event_title|striptags('<em>')|raw }}
            </a>
        </h{{ event_heading_level }}>

        {% if (event_sponsor) %}
        {% for sponsor in event_sponsor %}
        <p class="event__sponsor">
            {% if (sponsor.sponsor_website) %}
            <a href="{{ sponsor.sponsor_website|raw }}">{{ sponsor.name|raw }}</a>
            {% else %}
            {{ sponsor.name|raw }}
            {% endif %}
        </p>
        {% endfor %}
        {% endif %}

        {% if (event_time) %}
        <p class="event__time">
            {{ event_time|striptags('<em>')|raw }}
        </p>
        {% endif %}
        
        {% if (event_location) %}
        <p class="event__location">
            {{ event_location|striptags('<em>')|raw }}
        </p>
        {% endif %}
        
        <div class="event__excerpt">
            <p>{{ event_excerpt|striptags('<em>')|raw }}</p>
        </div>
        
        {% if (event_feature) %}</div>{% endif %}
        {% endif %}
    </div>
</div>