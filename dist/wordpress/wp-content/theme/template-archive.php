<?php
/**
 * Template Name: Archive
 *
 * @author Travis Cook (cooktw@missouri.edu), Undergraduate Studies, University of Missouri
 * @copyright 2019 Curators of the University of Missouri
 */

// Setup Timber
$aryContext = Timber::get_context();
$aryContext['page'] = new TimberPost();

// Map existing Timber option for permalink to alias
$aryContext['page']->current_page = $aryContext['page']->link;

// Body class
$aryContext['page']->body_class = 'archive';

// Sub-navigation 
if ((isset($aryContext['page']->sub_navigation)) && ($aryContext['page']->sub_navigation !== false)) {
    $aryContext['page']->sub_navigation = MizzouSite::getMenu($aryContext['page']->sub_navigation);
}

// Get settings
$archiveCPT = get_field('archive_cpt');

if ($archiveCPT == 'event') {
    $aryTax = get_field('archive_event_tax')->name;

    $aryParams = array(
        'post_type'         => 'event',
        'posts_per_page'    => 15,
        'meta_key'		    => 'end_date',
        'orderby'		    => 'meta_value_num',
        'order'			    => 'ASC',
        'paged'             => $paged,
        'meta_query' => array(
            array(
               'key'		=> 'end_date',
               'compare'	=> '>=',
               'value'		=> $today,
           )
       ),
        'tax_query'         => $aryTaxParams,
    );
    
    $aryTaxParams[] = array(
        array(
            'taxonomy'  => 'event_sponsor',
            'field'     => 'slug',
            'terms'     => $aryTax
        ),
    );

    $aryContext['events'] = Timber::get_posts($aryParams);
} elseif ($archiveCPT == 'staff') {
    $aryParams = array(
        'post_type'         => 'staff',
        'posts_per_page'    => 15,
        'paged'             => $paged,
    );

    $aryContext['staff_list'] = Timber::get_posts($aryParams);
} else {
    $aryTax = get_field('archive_tax')->name;

    $aryParams = array(
        'post_type'         => 'post',
        'posts_per_page'    => 15,
        'paged'             => $paged,
        'category_name'     => $aryTax,
    );
    $aryContext['news'] = Timber::get_posts($aryParams);
}

// Pagination
global $paged;
if (!isset($paged) || !$paged){
    $paged = 1;
}

$argsPagination = array(
    'mid_size'  => 1,
    'end_size'  => 1
);

query_posts($aryParams);
$aryContext['pagination'] = Timber::get_pagination($argsPagination);

// Render view
Timber::render('template-archive.twig', $aryContext);