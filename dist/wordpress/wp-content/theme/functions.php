<?php
/**
 * Functions and Definitions
 *
 * @author Josh Hughes (hughesjd@missouri.edu), Travis Cook (cooktw@missouri.edu), Undergraduate Studies, University of Missouri
 * @copyright 2018 Curators of the University of Missouri
 */

// Load defaults
require_once(__DIR__ . '/functions-defaults.php');

// Load Options
require_once(__DIR__ . '/options.php');

class MizzouSite extends StandardMizzouSite
{
    public function __construct()
	{
		// Add configuration variables
        add_filter('timber_context', array($this, 'siteConfiguration'));
        
        // Setup menu locations
        add_action('init', array($this, 'customMenuLocation'));
		
		// Setup post types and taxonomies
        add_action('init', array($this, 'customPostTypes'));
        add_action('init', array($this, 'customTaxonomies'));

        // Edit Dashboard Menu
        add_action('admin_menu', array($this, 'customDashboardMenu'));
        
        // Continue on
        parent::__construct();
	}

    /**
     * Setup configuration variables for site
     *
     * @param array $aryContext Parent context variable
     * @return Updated $aryContext
     */
    public function siteConfiguration($aryContext)
    {
        // Call parent
        parent::siteConfiguration($aryContext);
        
        // Standard configuration options
        $aryContext['site']->detected_hostname = $_SERVER['HTTP_HOST'];
        $aryContext['site']->google_analytics = get_option('google_analytics');
        $aryContext['site']->google_search_console = get_option('google_search_console');
        $aryContext['site']->google_cse_id = get_option('google_cse_id');
        $aryContext['site']->published_by = get_option('publisher_name');
        $aryContext['site']->published_by_link = get_option('publisher_url');
        $aryContext['site']->search_action_path = '';
        $aryContext['site']->search_enabled = true;
        $aryContext['site']->search_field_name = 's';
        $aryContext['site']->search_form_field_name = 's';
        $aryContext['site']->typekit_id = get_option('typekit_id');
        $aryContext['site']->viewport = 'width=device-width, initial-scale=1.0, shrink-to-fit=no';
        $aryContext['site']->year = date('Y');

        // Site specific configuration options
        $aryContext['site']->asset_version_number = '';
        $aryContext['site']->description = '';
        $aryContext['site']->hostname = '';
        $aryContext['site']->short_name = '';
        $aryContext['site']->subhead_name = get_option('subhead_name');
        $aryContext['site']->subhead_url = get_option('subhead_url');
        
        // Color Settings
        $aryContext['site']->apple_mask_icon_color = '#000000';
        $aryContext['site']->ms_tile_color = '#f1b82d';
        $aryContext['site']->site_icon_color = 'gold';
        
        // Social Media
        $aryContext['site']->facebook_url = get_option('facebook_username');
        $aryContext['site']->flickr_url = get_option('flickr_url');
        $aryContext['site']->google_username = get_option('google_username');
        $aryContext['site']->instagram_username = get_option('instagram_username');
        $aryContext['site']->linkedin_username = get_option('linkedin_username');
        $aryContext['site']->pinterest_url = get_option('pinterest_url');
        $aryContext['site']->snapchat_username = get_option('snapchat_username');
        $aryContext['site']->twitter_username = get_option('twitter_url');
        $aryContext['site']->youtube_url = get_option('youtube_url');
        
        // Navigation
        $aryContext['site']->navigation = $this->getMenuLoc('primary-navigation');
        $aryContext['site']->footer_navigation = $this->getMenuLoc('footer-navigation');
        $aryContext['site']->tactical_navigation = $this->getMenuLoc('tactical-navigation');

        // Continue on
        return $aryContext;
    }
    
    /**
     * Custom Dashboard Menu
     */
     public function customDashboardMenu()
     {
         // Remove Comments Menu
         remove_menu_page( 'edit-comments.php' );
     }

    /**
     * Setup custom menu locations
     */
     public function customMenuLocation()
     {
         register_nav_menu('primary-navigation',__( 'Primary Navigation' ));
         register_nav_menu('tactical-navigation',__( 'Tactical Navigation' ));
         register_nav_menu('footer-navigation',__( 'Footer Navigation' ));
     }

    /**
     * Setup custom taxonomies
     */
     public function customTaxonomies()
     {
        // Call parent
        parent::customTaxonomies();
     }

    /**
     * Setup custom post types
     */
    public function customPostTypes()
    {
        // Call parent
        parent::customPostTypes();
    }

    /**
     * Setup custom sort for staff
     */
    public function customStaffSort()
    {
        $orderby_statement = "menu_order DESC, RIGHT(post_title, LOCATE(' ', REVERSE(post_title)) - 1) ASC";
        return $orderby_statement;
    }
}

// Create object
new MizzouSite();