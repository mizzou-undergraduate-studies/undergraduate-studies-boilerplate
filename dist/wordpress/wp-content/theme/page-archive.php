<?php
/**
 * Archive Page
 *
 * @author Travis Cook (cooktw@missouri.edu), Undergraduate Studies, University of Missouri
 * @copyright 2017 Curators of the University of Missouri
 */

// Setup Timber
$aryContext = Timber::get_context();
$aryContext['page'] = new TimberPost();

// Map existing Timber option for permalink to alias
$aryContext['page']->current_page = $aryContext['page']->link;

// Body class
$aryContext['page']->body_class = 'archive';

// News
$argsNewsParams = array(
    'posts_per_page'    => 4,
    'post_type'         => 'post',
    'orderby'           => 'date',
    'order'             => 'DESC',
    'paged'             => $paged
);
$aryContext['news'] = Timber::get_posts($argsNewsParams);

// Feature story
$aryContext['feature'] = Timber::get_posts(array(
    'posts_per_page'    => 1,
    'category_name'     => 'featured'
));

// Get feature story id
if (isset($aryContext['feature'][0]->id)) {
    $aryNewsParams['post__not_in'] = array($aryContext['feature'][0]->id);
}

// Pagination
global $paged;
if (!isset($paged) || !$paged){
    $paged = 1;
}

$argsPagination = array(
    'mid_size'  => 1,
    'end_size'  => 1
);

query_posts($argsNewsParams);
$aryContext['pagination'] = Timber::get_pagination($argsPagination);

// Render view
Timber::render('page-archive.twig', $aryContext);