<?php
/**
 * Pages and Posts
 *
 * @author Josh Hughes (hughesjd@missouri.edu), Travis Cook (cooktw@missouri.edu), Undergraduate Studies, University of Missouri
 * @copyright 2018 Curators of the University of Missouri
 */

// Setup Timber
$aryContext = Timber::get_context();
$aryContext['page'] = new TimberPost();

// Map existing Timber option for permalink to alias
$aryContext['page']->current_page = $aryContext['page']->link;

// Collect parent links
$aryParents = array();
$objParent = $aryContext['page']->parent;
while ($objParent) {
    if ($objParent) {
        $aryParents[] = $objParent->link;
        $objParent = $objParent->parent;
    }
}
$aryContext['page']->parent_pages = $aryParents;

// Sub-navigation
if ((isset($aryContext['page']->sub_navigation)) && ($aryContext['page']->sub_navigation !== false)) {
    $aryContext['page']->sub_navigation = MizzouSite::getMenu($aryContext['page']->sub_navigation);
}

// Page or post
if ((isset($aryContext['page']->post_type)) && ($aryContext['page']->post_type == 'page')) {
    $strTemplatePrefix = 'page';
} else if ((isset($aryContext['page']->post_type)) && ($aryContext['page']->post_type == 'event')) {
    $strTemplatePrefix = 'single-event';
} else if ((isset($aryContext['page']->post_type)) && ($aryContext['page']->post_type == 'staff')) {
    $strTemplatePrefix = 'single-staff';
} else {
    $strTemplatePrefix = 'post';
}

// Create template hierarchy (will load first template found in the list)
$aryTemplates = array();

// Custom template
if (isset($aryContext['page']->slug)) {
    $aryTemplates[] = $strTemplatePrefix . '-' . $aryContext['page']->slug . '.twig';
}

// Custom parent template
if (isset($aryContext['page']->parent->slug)) {
    $aryTemplates[] = $strTemplatePrefix . '-' . $aryContext['page']->parent->slug . '.twig';
}

// Custom post type template
if ((isset($aryContext['page']->post_type)) && (!in_array($aryContext['page']->post_type, array('post', 'page'))))  {
    $aryTemplates[] = $strTemplatePrefix . '-' . $aryContext['page']->post_type . '.twig';
}

// Default
$aryTemplates[] = 'site-' . $strTemplatePrefix . '.twig';
$aryTemplates[] = $strTemplatePrefix . '.twig';

// If post type is 'block' and the user is not logged in, trigger a 404
if ((isset($aryContext['page']->post_type)) && ($aryContext['page']->post_type == 'block') && (!is_user_logged_in())) {
    get_template_part('404');
} else {

    // Render view
    Timber::render($aryTemplates, $aryContext);
}