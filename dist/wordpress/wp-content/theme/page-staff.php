<?php
/**
 * Staff Page
 *
 * @author Josh Hughes (hughesjd@missouri.edu), Travis Cook (cooktw@missouri.edu), Undergraduate Studies, University of Missouri
 * @copyright 2018 Curators of the University of Missouri
 */

// Setup Timber
$aryContext = Timber::get_context();
$aryContext['page'] = new TimberPost();

// Map existing Timber option for permalink to alias
$aryContext['page']->current_page = $aryContext['page']->link;

// Sub-navigation 
if ((isset($aryContext['page']->sub_navigation)) && ($aryContext['page']->sub_navigation !== false)) {
    $aryContext['page']->sub_navigation = MizzouSite::getMenu($aryContext['page']->sub_navigation);
}

/**
 * Setup custom sort for staff
 */
function customStaffSort()
{
    $orderby_statement = "menu_order DESC, RIGHT(post_title, LOCATE('-', REVERSE(post_name)) - 1) ASC";
    return $orderby_statement;
}

add_filter( 'posts_orderby' , 'customStaffSort' );
// Staff
$aryContext['staff_list'] = Timber::get_posts(array(
    'posts_per_page'    => -1,
    'post_type'         => 'staff',
));

remove_filter('posts_orderby' , 'customStaffSort');

// Render view
Timber::render('page-staff.twig', $aryContext);