![Undergraduate Studies Boilerplate](README.banner.png)

# Undergraduate Studies Boilerplate

This contains starter resources for most Undergraduate Studies projects.

## Installation

This project is setup to be used with the following tools to manage dependencies and various automation tasks (including compiling [Sass](http://sass-lang.com)):

- [Node JS](http://nodejs.org)
- [Bower](http://bower.io)
- [Gulp](http://gulpjs.com)

So, to start a new project using those tools, install them as directed by their websites, and then follow these instructions.

1. Create a folder for your project.
2. If you're creating a WordPress site open `start/wordpress/`, or if you're creating a static site open `start/static/`.
3. Copy `bower.json`, `gulpfile.js`, and `package.json` into your project folder.
4. You'll want to edit those three files to meet your project requirements, but at a minimum edit the `name`, `author`, `description`, and `repository` fields of `bower.json` and `package.json`.
5. Copy `src/scss/style.scss` to `src/scss/style.scss` in your project folder.
4. Open a Terminal, navigate to the folder, and run the following commands:

```
#!bash
npm install
bower install
gulp
```

That gets you most of the way there.

It's also not a bad idea to take a look at the [Automation and Dependencies](https://bitbucket.org/mizzou-undergraduate-studies/undergraduate-studies-documentation/src/master/dist/Automation-and-Dependencies.md) document in the [Undergraduate Studies Documentation](https://bitbucket.org/mizzou-undergraduate-studies/undergraduate-studies-documentation) repository.

## Detailed Content Description

Here's a detailed description of what's contained in this project.

### .htaccess

Every website requires `.htaccess` files to make some customizations to the server configuration.

#### For Static Sites

- `dist/static/htaccess` (Should be renamed `.htaccess`. Enables gzip, sets up error documents, and disables Internet Explorer compatibility mode)

#### For WordPress Sites

- `dist/wordpress/htaccess` (Should be renamed `.htaccess`. Enables gzip,  disables Internet Explorer compatibility mode, and limits certain files to the on-campus network)
- `dist/wordpress/wp-admin/htaccess` (Should be renamed `.htaccess` and placed in `wp-admin`. Limits admin area to the on-campus network)

### CSS and Sass

A lot of the usage of these files is documented in the [Undergraduate Studies Style Guide](https://undergraduatestudies.missouri.edu/styleguide).

- `dist/css/text-only.css` (Compiled styles for old browsers; see `src/scss/text-only.scss`)
- `src/scss/_mizzou-alerts.scss` (Alert box styles)
- `src/scss/_mizzou-article.scss` (Article styles)
- `src/scss/_mizzou-base.scss` (Basic styles)
- `src/scss/_mizzou-buttons.scss` (Button styles)
- `src/scss/_mizzou-footer.scss` (Footer styles)
- `src/scss/_mizzou-forms.scss` (Form styles)
- `src/scss/_mizzou-grid-system-and-layout.scss` (Grid system and layout styles)
- `src/scss/_mizzou-navigation.scss` (Navigation styles)
- `src/scss/_mizzou-person-profile.scss` (Person profile styles)
- `src/scss/_mizzou-search-form.scss` (Search form styles)
- `src/scss/_mizzou-search-results.scss` (Search result styles)
- `src/scss/_mizzou-signatures.scss` (Unit and University Signature styles)
- `src/scss/_mizzou-skip-to-content-link.scss` (Styles for "Skip to Content" links)
- `src/scss/_mizzou-social-media.scss` (Social media icon styles)
- `src/scss/_mizzou-stories.scss` (News and feature story styles)
- `src/scss/_mizzou-variables.scss` (Variables)
- `src/scss/editor-style.scss` (Starter for a Wordpress editor CSS file)
- `src/scss/style.scss` (Starter for a project-specific Sass file)
- `src/scss/text-only.scss` (Styles for old browsers)

### Twig and WordPress

We're now using [Twig](http://twig.sensiolabs.org)  for templating for both static and WordPress sites. 

#### Static Sites

- `dist/static/page.php` (Example page for a static Twig site)
- `dist/static/inc/twig-config.php` (Should be renamed `twig-config.php`. Example configuration)
- `dist/static/inc/twig-extensions.php` (Custom extensions for Twig; mostly for parsing navigation structures)
- `dist/static/inc/twig-setup.php` (Sets up Twig for use)

#### WordPress Sites

Usage on WordPress sites is enabled through the [Timber](http://upstatement.com/timber/) plugin. We do have a starter WordPress theme. This will expect a `templates` directory to be made a direct child of this folder.

- `dist/wordpress/wp-content/theme/404.php` (404 pages)
- `dist/wordpress/wp-content/theme/block.php` (Content blocks)
- `dist/wordpress/wp-content/theme/functions-defaults.php` (Default functions setup. Stuff we typically don't customize)
- `dist/wordpress/wp-content/theme/functions.php` (Site configuration. Covers a lot of the same ground as `dist/static/inc/twig-config.php` does for static sites)
- `dist/wordpress/wp-content/theme/index.php` (Homepage)
- `dist/wordpress/wp-content/theme/search.php` (Search)
- `dist/wordpress/wp-content/theme/singular.php` (Standard pages and posts)
- `dist/wordpress/wp-content/theme/style.css` (Used only for theme metadata)

#### Templates

- `dist/shared/inc/templates/404.html.twig` (404 page template)
- `dist/shared/inc/templates/components/block.html.twig` (Block template)
- `dist/shared/inc/templates/components/content.html.twig` (Content area template)
- `dist/shared/inc/templates/components/footer.html.twig` (Footer template)
- `dist/shared/inc/templates/components/form-field.html.twig` (Form field template)
- `dist/shared/inc/templates/components/google-analytics.html.twig` (Google Analytics template)
- `dist/shared/inc/templates/components/head-css.html.twig` (CSS setup template)
- `dist/shared/inc/templates/components/head-icons.html.twig` (Favicons, apple-touch-icons, and more template)
- `dist/shared/inc/templates/components/head-metadata.html.twig` (Metadata template)
- `dist/shared/inc/templates/components/header-with-university-signature.html.twig` (Standard header with a university signature template)
- `dist/shared/inc/templates/components/header.html.twig` (Standard header template)
- `dist/shared/inc/templates/components/legal-text.html.twig` (Legal text template)
- `dist/shared/inc/templates/components/navigation-pagination.html.twig` ("Previous"/"Next" template)
- `dist/shared/inc/templates/components/navigation.html.twig` (Navigation menu template)
- `dist/shared/inc/templates/components/outdated-browser.html.twig` (Outdated browser message template)
- `dist/shared/inc/templates/components/person-profile.html.twig` (Person profile template)
- `dist/shared/inc/templates/components/search-form.html.twig` (Search form template)
- `dist/shared/inc/templates/components/search-results.html.twig` (Search results template)
- `dist/shared/inc/templates/components/skip-to-content.html.twig` (Template for "Skip to Content" links)
- `dist/shared/inc/templates/components/social-media.html.twig` (Social media icon template)
- `dist/shared/inc/templates/components/story.html.twig` (News story template)
- `dist/shared/inc/templates/components/typekit.html.twig` (Typekit setup template)
- `dist/shared/inc/templates/components/unit-signature.html.twig` (Unit signature template)
- `dist/shared/inc/templates/components/university-signature.html.twig` (University signature template)
- `dist/shared/inc/templates/index.html.twig` (Homepage template)
- `dist/shared/inc/templates/macros/navigation.html.twig` (Macro for outputting menus)
- `dist/shared/inc/templates/main-template.html.twig` (Main template)
- `dist/shared/inc/templates/page.html.twig` (Standard page template)
- `dist/shared/inc/templates/post.html.twig` (Standard post template)
- `dist/shared/inc/templates/search.html.twig` (Search page)

### Images

Source files for all of these are available in `src/images/`

- `dist/shared/images/apple-pinned-site.svg` (Pinned site image for Safari on OS X El Capitan and greater)
- `dist/shared/images/favicon.ico` (Favicon)
- `dist/shared/images/microsoft-pinned-site.png` (Pinned site icon for Windows 8+)
- `dist/shared/images/mizzou-logo-*` (Mizzou logo variations)
- `dist/shared/images/search-icon-*` (Search icon variations)
- `dist/shared/images/site-icon-*` (Apple touch and social media icons)
- `dist/shared/images/social-media-*` (Social media icons and avatars)
- `dist/shared/images/unit-wordmark-*` (Unit signature wordmark variations)
- `dist/shared/images/university-wordmark-*` (University signature wordmark variations)

### JavaScript

- `src/js/class-manipulation.js` (jQuery-less class manipulation - See file for example usage)
- `src/js/config-google-analytics.js` (Google Analytics configuration)
- `src/js/dom-traversal.js` (jQuery-less DOM Traversal - See file for example usage)
- `src/js/menu-button.js` (Supports a "hamburger"-style mobile menu button)
- `vendor/google-analytics.js` (Google Analytics)

### PHP

- `dist/shared/inc/google-search.php` (Script for pulling GSA search results as XML)